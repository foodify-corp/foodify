# Foodify Project
___
## Summary
___
- ### Introduction

- ### Getting Start

  - **Requirements**
  - **Bash**
  - **Docker-compose**
  - **PHP**
  - **NODE**

- ### Package

- ### Documentation
  - **API**
- ### Roadmap
- ### Contributing
- ### Authors and acknowledgments
- ### License

## Introduction
___
Foodify is a web application that simplify the management of restaurant in many way possible.

This application provide you a way to manage your teams, your restaurant, your menu, your customer etc... .

This project is firstly split in two part, the back end, developed with symfony using API to communicate with
the frontend developed with the frameword Vue.js and Vuetify.

### Multiple interface are gonna be available for :

- Owner / Restaurant
- Manager
- Waiter
- Cook
- Kitchen
- External User
- Reviewer

## Getting started
To make the installation on your device, execute the following steps :

### Requirements

- Docker
- Docker-compose

#### Container Name:
- nginx
- php
- db
- node

### Project Installation

#### Bash

```bash
cd $PROJECT_PATH
cp .env.dist .env
```

#### Docker-compose

```bash
docker-compose build --pull --no-cache
docker-compose up -d
```
### Apply doctrine migration
```
bin/console doctrine:migrations:execute DoctrineMigrations\\Version20211215220718
```

#### PHP
```bash
docker-compose exec php composer install
docker-compose exec php bin/console doctrine:schema:update
docker-compose exec php bin/console doctrine:schema:drop --full-database --force # Drop Database

# Generate JWT Authentication public and private key
docker-compose exec php bin/console lexik:jwt:generate-keypair

# Fixtures
docker-compose exec php bin/console doctrine:fixtures:load
```

#### Node
```bash
docker-compose exec node npm install
docker-compose exec node npm run build # prod
docker-compose exec node npm run dev # dev
docker-compose exec node npm run dev-server # server webpack for css development
```

#### Package
| Package Name | Link |
| --- | --- |
| lexik/jwt-authentication-bundle | https://github.com/lexik/LexikJWTAuthenticationBundle |
| gesdinet/jwt-refresh-token-bundle | https://github.com/markitosgv/JWTRefreshTokenBundle |
| orm-fixtures | https://symfony.com/bundles/DoctrineFixturesBundle/current/index.html#installation |
| fzaninotto/faker | https://github.com/fzaninotto/Faker |
| vich/uploader-bundle | https://github.com/dustin10/VichUploaderBundle |
| gedmo/doctrine-extensions | https://github.com/doctrine-extensions/DoctrineExtensions |

### Informations
```
# URL
http://127.0.0.1

# Env example
DATABASE_URL="postgresql://postgres:password@db:5432/db?serverVersion=13&charset=utf8"
APP_ENV=dev
APP_SECRET=96f28b6f1952edec02941471e2fa40b9
```

### Help
```
- Duplication : 
https://stackoverflow.com/questions/39472899/migration-exception-alter-table-column-already-exists-1060-duplicate-colum
```
___

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:e92932bec0745c79c986033c0fda4875?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:e92932bec0745c79c986033c0fda4875?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:e92932bec0745c79c986033c0fda4875?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:e92932bec0745c79c986033c0fda4875?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:e92932bec0745c79c986033c0fda4875?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:e92932bec0745c79c986033c0fda4875?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:e92932bec0745c79c986033c0fda4875?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:e92932bec0745c79c986033c0fda4875?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:e92932bec0745c79c986033c0fda4875?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:e92932bec0745c79c986033c0fda4875?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


# API - Order

## Endpoint

### Create Order
#### POST - /api/restaurants/{id}/orders
Headers
```json
{
  "Content-type": "Application/json",
  "Authorization": "Bearer <JWT_token>"
}
```
Body  
*(order containing 2 differents dishes)*
```json
{
  "orderType": "On site",
  "orderDishes": [
    {
      "dish": {
        "name": "Burger de bison"
      },
      "quantity": 1
    },
    {
      "dish": {
        "name": "Salade du cowboy"
      },
      "quantity": 1
    }
  ]
}
```
#### Response
Headers
```json
{
  "Content-type": "Application/json",
  "status": 201
}
```
Body
```json
{
    "id": "6c604d97-5f94-433e-8a55-22bac8be7b1f",
    "order_type": "On site",
    "order_dishes": [
        {
            "quantity": 1,
            "dish": {
                "name": "Burger de bison",
                "price": 12
            }
        },
        {
            "quantity": 1,
            "dish": {
                "name": "Salade du cowboy",
                "price": 14.5
            }
        }
    ]
}
```


```
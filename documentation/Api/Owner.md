# API - Owner

## Endpoint

### /api/owner/{id}/restaurant - POST
#### Request
```json
{
  "Content-type": "Application/json"
}
```

```json
{
  "name": "string #required",
  "address": "string #required",
  "city": "string #required",
  "cp": "string #required",
  "country": "string #required",
  "activity": "string #required",
  "n_siren": "string #required",
  "kitchen_style": "string #required",
  "take_away": "boolean #required",
  "on_place": "boolean #required",
  "instagram_url": "string #optional",
  "facebook_url": "string #optional",
  "twitter_url": "string #optional"
}
```
#### Response

```json
{
  "Content-type": "Application/json",
  "status": 201
}
```
```json
{
  "user": "instance of User"
}
```

### /api/owner/{id}/restaurant/{restaurantId} - DELETE

#### Response
```json
{
  "Content-type": "Application/json",
  "status": 202
}
```

```json
{
  "restaurant": "The deleted restaurant"
}
```

### /api/owner/{id}/restaurant/{restaurantId} - UPDATE

#### Request
```json
{
  "Content-type": "Application/json"
}
```

```json
{
  "name": "string #required",
  "address": "string #required",
  "city": "string #required",
  "cp": "string #required",
  "country": "string #required",
  "activity": "string #required",
  "n_siren": "string #required",
  "kitchen_style": "string #required",
  "take_away": "boolean #required",
  "on_place": "boolean #required",
  "instagram_url": "string #optional",
  "facebook_url": "string #optional",
  "twitter_url": "string #optional"
}
```

#### Response
```json
{
  "Content-type": "Application/json",
  "status": 204
}
```
```json
{
  "restaurant": "The updated restaurant"
}
```
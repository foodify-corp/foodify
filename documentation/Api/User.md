# API - User

## Endpoint

### /api/login
#### Request
```json
{
  "Content-type": "Application/json",
}
```

```json
{
  "email": "string #required",
  "password": "string #required"
}
```
#### Response

```json
{
  "Content-type": "Application/json",
  "status": 201
}
```
```json
{
  "token": "string",
  "refresh_token": "string"
}
```

### /api/register
####Request

```json
{
  "Content-type": "Application/json"
}
```

```json
{
  "email": "string #required",
  "password": "string #required",
  "roles": [
    "string #optional"
  ],
  "firstname": "string #optional",
  "lastname": "string #optional",
  "birthday": "datetime #optional",
  "phone": "string #optional",
  "picture": "file #optional",
  "is_shown": "boolean #optional",
  "facebook_url": "string #optional",
  "instagram_url": "string #optional",
  "twitter_url": "string #optional"
}
```

#### Response
```json
{
  "Content-type": "Application/json",
  "status": 201
}
```

```json
{
  "status": "Customer created!"
}
```
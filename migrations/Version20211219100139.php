<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211219100139 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE IF NOT EXISTS "user" ADD firstname VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD lastname VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD birthday TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD phone VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD picture TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD is_shown BOOLEAN DEFAULT \'true\' NOT NULL');
        $this->addSql('ALTER TABLE "user" ADD facebook_url TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD instagram_url TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD twitter_url TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "user" DROP COLUMN firstname');
        $this->addSql('ALTER TABLE "user" DROP COLUMN lastname');
        $this->addSql('ALTER TABLE "user" DROP COLUMN birthday');
        $this->addSql('ALTER TABLE "user" DROP COLUMN phone');
        $this->addSql('ALTER TABLE "user" DROP COLUMN picture');
        $this->addSql('ALTER TABLE "user" DROP COLUMN is_shown');
        $this->addSql('ALTER TABLE "user" DROP COLUMN facebook_url');
        $this->addSql('ALTER TABLE "user" DROP COLUMN instagram_url');
        $this->addSql('ALTER TABLE "user" DROP COLUMN twitter_url');
    }
}

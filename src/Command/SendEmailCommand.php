<?php
//src/Command/SendEmailCommand.php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class SendEmailCommand extends Command
{
    protected static $defaultName = 'send:email';
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Command for send self email')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Command Send Self Email',
            '============'
        ]);

        // If you want use htmlTemplate and context function you need use TemplatedEmail
        // instead of Email object
        $email = (new Email())
            ->from('foodify.corp@gmail.com')
            ->to('l.antoine.h@gmail.com')
            ->priority(Email::PRIORITY_HIGH)
            ->subject('I love Me')
            // If you want use text mail only
            ->text('Lorem ipsum...')
            // If you want use raw html
            ->html('<h1>Hello World</h1> <p>...</p>')
            // if you want use template from your twig file
            // template/emails/registration.html.twig
            // with param
        ;
        $this->mailer->send($email);

        $output->writeln('Successful you send a self email');
        return Command::SUCCESS;

    }
}
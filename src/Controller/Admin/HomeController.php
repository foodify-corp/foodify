<?php

namespace App\Controller\Admin;

use App\Repository\ConnectedUserRepository;
use App\Repository\OrderRepository;
use App\Repository\ReservationRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use App\Service\DashboardConfigService;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class HomeController extends AbstractController
{
    const FILTER_NAME = "connected_users";
    const DASHBOARD_FILTER_SELECTION = [
        [
            "name" => ConnectedUserRepository::FILTER_NAME,
            "value" => ConnectedUserRepository::FILTER_VALUE
        ],
        [
            "name" => UserRepository::FILTER_NAME,
            "value" => UserRepository::FILTER_VALUE
        ],
        [
            "name" => RestaurantRepository::FILTER_NAME,
            "value" => RestaurantRepository::FILTER_VALUE
        ],
        [
            "name" => OrderRepository::FILTER_NAME,
            "value" => OrderRepository::FILTER_VALUE
        ],
        [
            "name" => ReservationRepository::FILTER_NAME,
            "value" => ReservationRepository::FILTER_VALUE
        ],

    ];

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    #[Route('/dashboard/{chart_selected}/{chart_filter}/{years}/{months}/{days}/{hours}', name: 'home_default', defaults: [ "chart_selected" => self::DASHBOARD_FILTER_SELECTION[0]["value"], "chart_filter" => DashboardConfigService::MINUTES, "years" => "", "months" => "", "days" => "", "hours" => "" ], methods: ["GET"])]
    public function dashboard(string $chart_selected, string $chart_filter, string $years, string $months, string $days, string $hours, ChartBuilderInterface $chartBuilder, UserRepository $userRepository, RestaurantRepository $restaurantRepository, ConnectedUserRepository $connectedUserRepository, OrderRepository $orderRepository, ReservationRepository $reservationRepository, EntityManagerInterface $em): Response
    {
        $now = CarbonImmutable::now();
        $years = $years?intval($years):$now->year;
        $months = $months?intval($months):$now->month;
        $days = $days?intval($days):$now->day;
        $hours = $hours?intval($hours):$now->hour;

        $chart = $chartBuilder->createChart(Chart::TYPE_BAR);
        $dashboard_config_service = New DashboardConfigService($em, $chart, $chart_selected, $chart_filter);
        $dashboard_config_service->addSelectionRange(self::DASHBOARD_FILTER_SELECTION);
        $dashboard_config_service->setChart($years, $months, $days, $hours);
        $bottom_value = [
            'Utilisateurs connectés' => $connectedUserRepository->getConnectedUser(),
            'Utilisateurs' => $userRepository->usersCount(),
            'Restaurants' => $restaurantRepository->restaurantsCount(),
            'Commandes' => $orderRepository->orderCount(),
            'Reservation' => $reservationRepository->reservationCount(),
        ];

        $dashboard_config_service->addBottomSelectionRange($bottom_value);

        return $this->render('admin/home/dashboard.html.twig', [
            'page_title' => 'Dashboard',
            'bottom_value' => $dashboard_config_service->getBottomSelection(),
            'years' => $years,
            'months' => $months,
            'days' => $days,
            'hours' => $hours,
            'chart' => $dashboard_config_service->getChart(),
            'chart_filter' => $chart_filter,
            'chart_selected' => $chart_selected,
            'chart_selection' => $dashboard_config_service->getSelection(),
            'chart_filter_parameter' => $dashboard_config_service->getFilter(),
            'latest_value' => $dashboard_config_service->getLatestValue(),
        ]);
    }
}

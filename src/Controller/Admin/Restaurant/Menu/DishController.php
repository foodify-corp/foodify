<?php

namespace App\Controller\Admin\Restaurant\Menu;

use App\Entity\Restaurant;
use App\Form\RestaurantType;
use App\Repository\DishRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use App\Service\DashboardConfigService;
use Carbon\CarbonImmutable;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[Route('/restaurant', name: 'restaurant_', methods: ["GET"])]
class DishController extends AbstractController
{
    const CONTROLLER_NAME = "Plat";

    const DASHBOARD_FILTER_SELECTION = [
        [
            "name" => DishRepository::FILTER_NAME,
            "value" => DishRepository::FILTER_VALUE
        ],
    ];

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    #[Route('/{restaurant}/menu/category/{chart_selected}/{chart_filter}/{years}/{months}/{days}/{hours}',
        name: 'category_dashboard',
        defaults: [ "chart_selected" => self::DASHBOARD_FILTER_SELECTION[0]["value"], "chart_filter" => DashboardConfigService::MINUTES, "years" => "", "months" => "", "days" => "", "hours" => "" ],
        methods: ["GET"])
    ]
    public function dashboard(Restaurant $restaurant, string $chart_selected, string $chart_filter, string $years, string $months, string $days, string $hours, DishRepository $dishRepository, ChartBuilderInterface $chartBuilder, EntityManagerInterface $em): Response
    {
        $now = CarbonImmutable::now();
        $years = $years?intval($years):$now->year;
        $months = $months?intval($months):$now->month;
        $days = $days?intval($days):$now->day;
        $hours = $hours?intval($hours):$now->hour;

        $chart = $chartBuilder->createChart(Chart::TYPE_BAR);
        $dashboard_config_service = New DashboardConfigService($em, $chart, $chart_selected, $chart_filter);
        $dashboard_config_service->addSelectionRange(self::DASHBOARD_FILTER_SELECTION);
        $dashboard_config_service->setChart($years, $months, $days, $hours);
        $bottom_value = [
            'Plat' => $dishRepository->dishCount(),
        ];

        $dashboard_config_service->addBottomSelectionRange($bottom_value);

        return $this->render('admin/restaurant/reservation/dashboard.html.twig', [
            'restaurant' => $restaurant,
            'restaurant_title' => ucfirst($restaurant->getName()) . ' / ' . self::CONTROLLER_NAME . ' / ' . 'Dashboard',
            'years' => $years,
            'months' => $months,
            'days' => $days,
            'hours' => $hours,
            'bottom_value' => $dashboard_config_service->getBottomSelection(),
            'chart' => $dashboard_config_service->getChart(),
            'chart_filter' => $chart_filter,
            'chart_selected' => $chart_selected,
            'chart_selection' => $dashboard_config_service->getSelection(),
            'chart_filter_parameter' => $dashboard_config_service->getFilter(),
            'latest_value' => $dashboard_config_service->getLatestValue(),
        ]);
    }

    #[Route('/{restaurant_id}/category/list', name: 'category_list', methods: ["GET"])]
    public function list(string $restaurant_id, RestaurantRepository $restaurantRepository): Response
    {
        $restaurantRepository->findAll();

        return $this->render('admin/restaurant/category/list.html.twig', [
            'restaurants' => $restaurantRepository->findAll(),
        ]);
    }

    #[Route('/{restaurant_id}/category/new', name: 'category_create', methods: ["GET", "POST"])]
    public function new(string $restaurant_id, Request $request, UserRepository $userRepository): Response
    {
        $restaurant = new Restaurant();
        $users = $userRepository->findAll();
        $users_options = array();

        foreach($users as $value) {
            $users_options[$value->getEmail()] = $value->getId();
        }

        $form = $this->createForm(RestaurantType::class, $restaurant, ["users_options" => $users_options]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($restaurant);
                $entityManager->flush();

                $this->addFlash('success', 'Restaurant ajouté avec succès!');
                return $this->redirectToRoute('admin_create_restaurant', [], Response::HTTP_CREATED);
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash('failure', 'Une erreur est survenue: '.$e->getMessage());
                return $this->redirectToRoute('admin_create_restaurant', [], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->renderForm('admin/restaurant/category/create.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{restaurant_id}/category/edit/{category_id}', name: 'category_edit', methods: ["GET"])]
    public function edit(string $restaurant_id, string $category_id): Response
    {
        return $this->render('admin/restaurant/category/index.html.twig', [
            'page_title' => 'Index Page',
            'controller_name' => 'Admin Back',
        ]);
    }

    #[Route('/{restaurant_id}/category/remove/{category_id}', name: 'category_remove', methods: ["GET"])]
    public function remove(string $restaurant_id, string $category_id, Restaurant $restaurant, $token): Response
    {
        if ($this->isCsrfTokenValid('remove_restaurant', $token)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($restaurant);
            $em->flush();

            $this->addFlash('success', "Le restaurant {$restaurant->getName()} à bien été supprimé.");

            return $this->redirectToRoute('admin_restaurant_default');
        }

        throw new Exception('Invalid token CSRF');
    }

    #[Route('/{restaurant_id}/category/{category_id}', name: 'category_show')]
    public function show(string $restaurant_id, string $category_id, Restaurant $restaurant): Response
    {
        return $this->render('admin/restaurant/category/show.html.twig', [
            'restaurant' => $restaurant
        ]);
    }

}

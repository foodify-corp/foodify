<?php

namespace App\Controller\Admin\Restaurant;

use App\Entity\Menu;
use App\Entity\Order;
use App\Entity\Restaurant;
use App\Form\OrderType;
use App\Repository\OrderRepository;
use App\Service\DashboardConfigService;
use Carbon\CarbonImmutable;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[Route('/restaurant', name: 'restaurant_', methods: ["GET"])]
class OrderController extends AbstractController
{
    const CONTROLLER_NAME = 'Commande';

    const DASHBOARD_FILTER_SELECTION = [
        [
            "name" => OrderRepository::FILTER_NAME,
            "value" => OrderRepository::FILTER_VALUE
        ],
    ];

    #[Route('/{id}/order/dashboard/{chart_selected}/{chart_filter}/{years}/{months}/{days}/{hours}',
        name: 'order_dashboard',
        defaults: [ "chart_selected" => self::DASHBOARD_FILTER_SELECTION[0]["value"], "chart_filter" => DashboardConfigService::MINUTES, "years" => "", "months" => "", "days" => "", "hours" => "" ],
        methods: ["GET"])
    ]
    public function dashboard(Restaurant $restaurant, string $chart_selected, string $chart_filter, string $years, string $months, string $days, string $hours, ChartBuilderInterface $chartBuilder, OrderRepository $orderRepository, EntityManagerInterface $em): Response
    {
        $now = CarbonImmutable::now();
        $years = $years?intval($years):$now->year;
        $months = $months?intval($months):$now->month;
        $days = $days?intval($days):$now->day;
        $hours = $hours?intval($hours):$now->hour;

        $chart = $chartBuilder->createChart(Chart::TYPE_BAR);
        $dashboard_config_service = New DashboardConfigService($em, $chart, $chart_selected, $chart_filter);
        $dashboard_config_service->addSelectionRange(self::DASHBOARD_FILTER_SELECTION);
        $dashboard_config_service->setChart($years, $months, $days, $hours);
        $bottom_value = [
            'Order' => $orderRepository->orderCount(),
        ];

        $dashboard_config_service->addBottomSelectionRange($bottom_value);

        return $this->render('admin/restaurant/order/dashboard.html.twig', [
            'restaurant' => $restaurant,
            'restaurant_title' => ucfirst($restaurant->getName()) . ' / ' . self::CONTROLLER_NAME . ' / ' . 'Dashboard',
            'years' => $years,
            'months' => $months,
            'days' => $days,
            'hours' => $hours,
            'bottom_value' => $dashboard_config_service->getBottomSelection(),
            'chart' => $dashboard_config_service->getChart(),
            'chart_filter' => $chart_filter,
            'chart_selected' => $chart_selected,
            'chart_selection' => $dashboard_config_service->getSelection(),
            'chart_filter_parameter' => $dashboard_config_service->getFilter(),
            'latest_value' => $dashboard_config_service->getLatestValueByRestaurant($restaurant, 'restaurant'),
        ]);
    }

    #[Route('/{id}/order/list', name: 'order_list', methods: ["GET"])]
    public function list(Restaurant $restaurant, OrderRepository $orderRepository): Response
    {
        $classMetaData = $this->getDoctrine()->getManager()->getClassMetadata($orderRepository->getClassName());
        $filterParameter = $classMetaData->getFieldNames();

        $filteredValues = [];

        foreach ($orderRepository->findAllToArrayByRestaurantId($restaurant->getId()) as $order) {
            if (!$filteredValues) {
                $filteredValues = $order;
            } else {
                $filteredValues = array_merge_recursive($filteredValues, $order);
            }
        }

        foreach ($filteredValues as $key => $value) {
            if (is_array($value)) {
                $filteredValues[$key] = array_unique($value, SORT_REGULAR);
            } else {
                $filteredValues[$key] = $value;
            }
        }

        return $this->render('admin/restaurant/order/list.html.twig', [
            'restaurant' => $restaurant,
            'entities' => $orderRepository->findAllToArrayByRestaurantId($restaurant->getId()),
            'filterParameters' => $filterParameter,
            'filterValues' => $filteredValues,
            'entityName' => $classMetaData->table['name'],
        ]);
    }

    #[Route('/{id}/order/new', name: 'order_create', methods: ["GET", "POST"])]
    public function new(Restaurant $restaurant, Request $request): Response
    {
        $order = new Order();

        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $order->setRestaurant($restaurant);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($order);
                $entityManager->flush();

                $this->addFlash('success', 'Menu ajouté avec succès!');
                return $this->redirectToRoute('admin_restaurant_order_list', ["id" => $restaurant->getId()], Response::HTTP_CREATED);
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash('failure', 'Une erreur est survenue: '.$e->getMessage());
                return $this->redirectToRoute('admin_restaurant_order_list', ["id" => $restaurant->getId()], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->renderForm('admin/restaurant/default/create.html.twig', [
            'restaurant' => $restaurant,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/order/edit/{entity}', name: 'order_edit', methods: ["GET"])]
    public function edit(Restaurant $restaurant, Order $entity): Response
    {
        return $this->render('admin/restaurant/order/index.html.twig', [
            'page_title' => 'Index Page',
            'controller_name' => 'Admin Back',
        ]);
    }

    #[Route('/{id}/order/remove/{entity}/{token}', name: 'order_remove', methods: ["GET"])]
    public function remove(Restaurant $restaurant, Order $entity, $token): Response
    {
        if ($this->isCsrfTokenValid('remove_restaurant', $token)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($restaurant);
            $em->flush();

            $this->addFlash('success', "Le restaurant {$restaurant->getName()} à bien été supprimé.");

            return $this->redirectToRoute('admin_restaurant_default');
        }

        throw new Exception('Invalid token CSRF');
    }

    #[Route('/{id}/order/{entity}', name: 'order_show')]
    public function show(Restaurant $restaurant, Order $entity): Response
    {
        return $this->render('admin/restaurant/order/show.html.twig', [
            'restaurant' => $restaurant
        ]);
    }

}

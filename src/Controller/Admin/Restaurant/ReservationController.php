<?php

namespace App\Controller\Admin\Restaurant;

use App\Entity\Reservation;
use App\Entity\Restaurant;
use App\Form\ReservationType;
use App\Form\RestaurantType;
use App\Repository\MenuRepository;
use App\Repository\ReservationRepository;
use App\Repository\UserRepository;
use App\Service\DashboardConfigService;
use Carbon\CarbonImmutable;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[Route('/restaurant', name: 'restaurant_', methods: ["GET"])]
class ReservationController extends AbstractController
{
    const CONTROLLER_NAME = 'Réservation';

    const DASHBOARD_FILTER_SELECTION = [
        [
            "name" => MenuRepository::FILTER_NAME,
            "value" => MenuRepository::FILTER_VALUE
        ],
    ];

    #[Route('/{id}/reservation/dashboard/{chart_selected}/{chart_filter}/{years}/{months}/{days}/{hours}',
        name: 'reservation_dashboard',
        defaults: [ "chart_selected" => self::DASHBOARD_FILTER_SELECTION[0]["value"], "chart_filter" => DashboardConfigService::MINUTES, "years" => "", "months" => "", "days" => "", "hours" => "" ],
        methods: ["GET"])
    ]
    public function dashboard(Restaurant $restaurant, string $chart_selected, string $chart_filter, string $years, string $months, string $days, string $hours, ReservationRepository $reservationRepository, ChartBuilderInterface $chartBuilder, EntityManagerInterface $em): Response
    {
        $now = CarbonImmutable::now();
        $years = $years?intval($years):$now->year;
        $months = $months?intval($months):$now->month;
        $days = $days?intval($days):$now->day;
        $hours = $hours?intval($hours):$now->hour;

        $chart = $chartBuilder->createChart(Chart::TYPE_BAR);
        $dashboard_config_service = New DashboardConfigService($em, $chart, $chart_selected, $chart_filter);
        $dashboard_config_service->addSelectionRange(self::DASHBOARD_FILTER_SELECTION);
        $dashboard_config_service->setChart($years, $months, $days, $hours);
        $bottom_value = [
            'Réservation' => $reservationRepository->reservationCount(),
        ];

        $dashboard_config_service->addBottomSelectionRange($bottom_value);

        return $this->render('admin/restaurant/reservation/dashboard.html.twig', [
            'restaurant' => $restaurant,
            'restaurant_title' => ucfirst($restaurant->getName()) . ' / ' . self::CONTROLLER_NAME . ' / ' . 'Dashboard',
            'years' => $years,
            'months' => $months,
            'days' => $days,
            'hours' => $hours,
            'bottom_value' => $dashboard_config_service->getBottomSelection(),
            'chart' => $dashboard_config_service->getChart(),
            'chart_filter' => $chart_filter,
            'chart_selected' => $chart_selected,
            'chart_selection' => $dashboard_config_service->getSelection(),
            'chart_filter_parameter' => $dashboard_config_service->getFilter(),
            'latest_value' => $dashboard_config_service->getLatestValueByRestaurant($restaurant, 'restaurant_id'),
        ]);
    }

    #[Route('/{id}/reservation/list', name: 'reservation_list', methods: ["GET"])]
    public function list(Restaurant $restaurant, ReservationRepository $reservationRepository): Response
    {
        $classMetaData = $this->getDoctrine()->getManager()->getClassMetadata($reservationRepository->getClassName());
        $filterParameter = $classMetaData->getFieldNames();

        $filteredValues = [];

        foreach ($reservationRepository->findAllToArrayByRestaurantId($restaurant->getId()) as $reservation) {
            if (!$filteredValues) {
                $filteredValues = $reservation;
            } else {
                $filteredValues = array_merge_recursive($filteredValues, $reservation);
            }
        }

        foreach ($filteredValues as $key => $value) {
            if (is_array($value)) {
                $filteredValues[$key] = array_unique($value, SORT_REGULAR);
            } else {
                $filteredValues[$key] = $value;
            }
        }

        return $this->render('admin/restaurant/order/list.html.twig', [
            'restaurant' => $restaurant,
            'entities' => $reservationRepository->findAllToArrayByRestaurantId($restaurant->getId()),
            'filterParameters' => $filterParameter,
            'filterValues' => $filteredValues,
            'entityName' => $classMetaData->table['name'],
        ]);
    }

    #[Route('/{id}/reservation/new', name: 'reservation_create', methods: ["GET", "POST"])]
    public function new(Restaurant $restaurant, Request $request): Response
    {
        $reservation = new Reservation();

        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $reservation->setRestaurantId($restaurant);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($reservation);
                $entityManager->flush();

                $this->addFlash('success', 'Reservation ajouté avec succès!');
                return $this->redirectToRoute('admin_restaurant_reservation_list', [], Response::HTTP_CREATED);
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash('failure', 'Une erreur est survenue: '.$e->getMessage());
                return $this->redirectToRoute('admin_restaurant_reservation_list', [], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->renderForm('admin/restaurant/default/create.html.twig', [
            'restaurant' => $restaurant,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/reservation/edit/{entity}', name: 'reservation_edit', methods: ["GET"])]
    public function edit(Restaurant $restaurant, Reservation $entity): Response
    {
        return $this->render('admin/restaurant/reservation/index.html.twig', [
            'restaurant' => $restaurant,
            'page_title' => 'Index Page',
            'controller_name' => 'Admin Back',
        ]);
    }

    #[Route('/{id}/reservation/remove/{entity}', name: 'reservation_remove', methods: ["GET"])]
    public function remove(Restaurant $restaurant, Reservation $entity, $token): Response
    {
        if ($this->isCsrfTokenValid('remove_restaurant', $token)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($restaurant);
            $em->flush();

            $this->addFlash('success', "Le restaurant {$restaurant->getName()} à bien été supprimé.");

            return $this->redirectToRoute('admin_restaurant_default');
        }

        throw new Exception('Invalid token CSRF');
    }

    #[Route('/{id}/reservation/{entity}', name: 'reservation_show')]
    public function show(Restaurant $restaurant, Reservation $entity): Response
    {
        return $this->render('admin/restaurant/reservation/show.html.twig', [
            'restaurant' => $restaurant
        ]);
    }

}

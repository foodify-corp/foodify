<?php

namespace App\Controller\Admin\Restaurant;

use App\Entity\Restaurant;
use App\Entity\User;
use App\Form\RestaurantType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Repository\UserWorkAtRestaurantRepository;
use App\Service\DashboardConfigService;
use Carbon\CarbonImmutable;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[Route('/restaurant', name: 'restaurant_', methods: ["GET"])]
class UserController extends AbstractController
{
    const CONTROLLER_NAME = 'Utilisateur';

    const DASHBOARD_FILTER_SELECTION = [
        [
            "name" => UserRepository::FILTER_NAME,
            "value" => UserRepository::FILTER_VALUE
        ],
    ];

    #[Route('/{id}/user/dashboard/{chart_selected}/{chart_filter}/{years}/{months}/{days}/{hours}',
        name: 'user_dashboard',
        defaults: [ "chart_selected" => self::DASHBOARD_FILTER_SELECTION[0]["value"], "chart_filter" => DashboardConfigService::MINUTES, "years" => "", "months" => "", "days" => "", "hours" => "" ],
        methods: ["GET"])
    ]
    public function dashboard(Restaurant $restaurant, string $chart_selected, string $chart_filter, string $years, string $months, string $days, string $hours, UserRepository $userRepository, ChartBuilderInterface $chartBuilder, EntityManagerInterface $em): Response
    {
        $now = CarbonImmutable::now();
        $years = $years?intval($years):$now->year;
        $months = $months?intval($months):$now->month;
        $days = $days?intval($days):$now->day;
        $hours = $hours?intval($hours):$now->hour;

        $chart = $chartBuilder->createChart(Chart::TYPE_BAR);
        $dashboard_config_service = New DashboardConfigService($em, $chart, $chart_selected, $chart_filter);
        $dashboard_config_service->addSelectionRange(self::DASHBOARD_FILTER_SELECTION);
        $dashboard_config_service->setChart($years, $months, $days, $hours);
        $bottom_value = [
            'User' => $userRepository->usersCount(),
        ];

        $dashboard_config_service->addBottomSelectionRange($bottom_value);

        return $this->render('admin/restaurant/user/dashboard.html.twig', [
            'restaurant' => $restaurant,
            'restaurant_title' => ucfirst($restaurant->getName()) . ' / ' . self::CONTROLLER_NAME . ' / ' . 'Dashboard ',
            'page_title' => 'Index Page',
            'years' => $years,
            'months' => $months,
            'days' => $days,
            'hours' => $hours,
            'bottom_value' => $dashboard_config_service->getBottomSelection(),
            'chart' => $dashboard_config_service->getChart(),
            'chart_filter' => $chart_filter,
            'chart_selected' => $chart_selected,
            'chart_selection' => $dashboard_config_service->getSelection(),
            'chart_filter_parameter' => $dashboard_config_service->getFilter(),
            'latest_value' => $dashboard_config_service->getLatestValue(),
        ]);
    }

    #[Route('/{id}/user/list', name: 'user_list', methods: ["GET"])]
    public function list(Request $request, Restaurant $restaurant, UserWorkAtRestaurantRepository $userWorkAtRestaurantRepository): Response
    {
        $entities = [];

        if ( $request->query->all() ) {
            $entities = $userWorkAtRestaurantRepository->findAllToArrayByParametersByRestaurant($request->query->all(), $restaurant->getId());
        } else {
            $entities = $userWorkAtRestaurantRepository->findAllToArray();
        }

        $classMetaData = $this->getDoctrine()->getManager()->getClassMetadata($userWorkAtRestaurantRepository->getClassName());
        $filterParameter = $classMetaData->getFieldNames();

        $filteredValues = [];

        foreach ($userWorkAtRestaurantRepository->findAllToArrayByRestaurantId($restaurant->getId()) as $user) {
            if (!$filteredValues) {
                $filteredValues = $user;
            } else {
                $filteredValues = array_merge_recursive($filteredValues, $user);
            }
        }

        foreach ($filteredValues as $key => $value) {
            if (is_array($value)) {
                $filteredValues[$key] = array_unique($value, SORT_REGULAR);
            } else {
                $filteredValues[$key] = $value;
            }
        }

        return $this->render('admin/restaurant/order/list.html.twig', [
            'restaurant' => $restaurant,
            'entities' => $entities,
            'filterParameters' => $filterParameter,
            'filterValues' => $filteredValues,
            'entityName' => $classMetaData->table['name'],
        ]);
    }

    #[Route('/{id}/user/new', name: 'user_create', methods: ["GET", "POST"])]
    public function new(Restaurant $restaurant, Request $request, UserRepository $userRepository): Response
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($restaurant);
                $entityManager->flush();

                $this->addFlash('success', 'Restaurant ajouté avec succès!');
                return $this->redirectToRoute('admin_create_restaurant', [], Response::HTTP_CREATED);
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash('failure', 'Une erreur est survenue: '.$e->getMessage());
                return $this->redirectToRoute('admin_create_restaurant', [], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->renderForm('admin/restaurant/default/create.html.twig', [
            'restaurant' => $restaurant,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/user/edit/{entity}', name: 'user_edit', methods: ["GET"])]
    public function edit(Restaurant $restaurant, User $entity): Response
    {
        return $this->render('admin/restaurant/user/index.html.twig', [
            'page_title' => 'Index Page',
            'controller_name' => 'Admin Back',
        ]);
    }

    #[Route('/{id}/user/remove/{entity}', name: 'user_remove', methods: ["GET"])]
    public function remove(Restaurant $restaurant, User $entity, $token): Response
    {
        if ($this->isCsrfTokenValid('remove_restaurant', $token)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($restaurant);
            $em->flush();

            $this->addFlash('success', "Le restaurant {$restaurant->getName()} à bien été supprimé.");

            return $this->redirectToRoute('admin_restaurant_default');
        }

        throw new Exception('Invalid token CSRF');
    }

    #[Route('/{id}/user/{entity}', name: 'user_show')]
    public function show(Restaurant $restaurant, User $entity): Response
    {
        return $this->render('admin/restaurant/user/show.html.twig', [
            'restaurant' => $restaurant
        ]);
    }

}

<?php

namespace App\Controller\Admin;

use App\Entity\Restaurant;
use App\Form\RestaurantType;
use App\Repository\MenuRepository;
use App\Repository\OrderRepository;
use App\Repository\ReservationRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use App\Service\DashboardConfigService;
use Carbon\CarbonImmutable;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class RestaurantController extends AbstractController
{
    const DASHBOARD_FILTER_SELECTION = [
        [
            "name" => RestaurantRepository::FILTER_NAME,
            "value" => RestaurantRepository::FILTER_VALUE
        ],
        [
            "name" => OrderRepository::FILTER_NAME,
            "value" => OrderRepository::FILTER_VALUE
        ],
        [
            "name" => ReservationRepository::FILTER_NAME,
            "value" => ReservationRepository::FILTER_VALUE
        ],
        [
            "name" => MenuRepository::FILTER_NAME,
            "value" => MenuRepository::FILTER_VALUE
        ],
    ];

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    #[Route('/restaurant/dashboard/{chart_selected}/{chart_filter}/{years}/{months}/{days}/{hours}',
        name: 'restaurant_dashboard',
        defaults: [ "chart_selected" => self::DASHBOARD_FILTER_SELECTION[0]["value"], "chart_filter" => DashboardConfigService::MINUTES, "years" => "", "months" => "", "days" => "", "hours" => "" ],
        methods: ["GET"])
    ]
    public function dashboard(string $chart_selected, string $chart_filter, string $years, string $months, string $days, string $hours, ChartBuilderInterface $chartBuilder, RestaurantRepository $restaurantRepository, OrderRepository $orderRepository, ReservationRepository $reservationRepository, MenuRepository $menuRepository, EntityManagerInterface $em): Response
    {
        $now = CarbonImmutable::now();
        $years = $years?intval($years):$now->year;
        $months = $months?intval($months):$now->month;
        $days = $days?intval($days):$now->day;
        $hours = $hours?intval($hours):$now->hour;

        $chart = $chartBuilder->createChart(Chart::TYPE_BAR);
        $dashboard_config_service = New DashboardConfigService($em, $chart, $chart_selected, $chart_filter);
        $dashboard_config_service->addSelectionRange(self::DASHBOARD_FILTER_SELECTION);
        $dashboard_config_service->setChart($years, $months, $days, $hours);
        $bottom_value = [
            'Restaurant' => $restaurantRepository->restaurantsCount(),
            'Commande' => $orderRepository->orderCount(),
            'Réservation' => $reservationRepository->reservationCount(),
            'Menu' => $menuRepository->menuCount(),
        ];

        $dashboard_config_service->addBottomSelectionRange($bottom_value);

        return $this->render('admin/restaurant/dashboard.html.twig', [
            'bottom_value' => $dashboard_config_service->getBottomSelection(),
            'years' => $years,
            'months' => $months,
            'days' => $days,
            'hours' => $hours,
            'chart' => $dashboard_config_service->getChart(),
            'chart_filter' => $chart_filter,
            'chart_selected' => $chart_selected,
            'chart_selection' => $dashboard_config_service->getSelection(),
            'chart_filter_parameter' => $dashboard_config_service->getFilter(),
            'latest_value' => $dashboard_config_service->getLatestValue(),
        ]);
    }

    /**
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    #[NoReturn] #[Route('/restaurant/list', name: 'restaurant_list', methods: ["GET"])]
    public function list(Request $request, RestaurantRepository $restaurantRepository): Response
    {
        $entities = [];

        if ( $request->query->all() ) {
            $entities = $restaurantRepository->findAllToArrayByParameters($request->query->all());
        } else {
            $entities = $restaurantRepository->findAllToArray();
        }

        $classMetaData = $this->getDoctrine()->getManager()->getClassMetadata($restaurantRepository->getClassName());
        $filterParameter = $classMetaData->getFieldNames();

        $filteredValues = [];

        foreach ($restaurantRepository->findAllToArray() as $restaurant) {
            if (!$filteredValues) {
                $filteredValues = $restaurant;
            } else {
                $filteredValues = array_merge_recursive($filteredValues, $restaurant);
            }
        }

        foreach ($filteredValues as $key => $value) {
            if (is_array($value)) {
                $filteredValues[$key] = array_unique($value, SORT_REGULAR);
            } else {
                $filteredValues[$key] = $value;
            }
        }

        return $this->render('admin/restaurant/list.html.twig', [
            'entities' => $entities,
            'filterParameters' => $filterParameter,
            'filterValues' => $filteredValues,
            'entityName' => $classMetaData->table['name'],
        ]);
    }

    #[Route('/restaurant/new', name: 'restaurant_create', methods: ["GET", "POST"])]
    public function new(Request $request): Response
    {
        $restaurant = new Restaurant();

        $form = $this->createForm(RestaurantType::class, $restaurant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($restaurant);
                $entityManager->flush();

                $this->addFlash('success', 'Restaurant ajouté avec succès!');
                return $this->redirectToRoute('admin_restaurant_create', [], Response::HTTP_CREATED);
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash('failure', 'Une erreur est survenue: '.$e->getMessage());
                return $this->redirectToRoute('admin_restaurant_create', [], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->renderForm('admin/restaurant/create.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/restaurant/edit/{id}', name: 'restaurant_edit', methods: ['GET','POST'])]
    public function edit(Restaurant $restaurant, Request $request): Response
    {
        $form = $this->createForm(RestaurantType::class, $restaurant);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('green', "Le lieu {$restaurant->getName()} à bien été édité.");

            return $this->redirectToRoute('admin_restaurant_edit', [
                'id' => $restaurant->getId()
            ]);
        }

        return $this->render('admin/restaurant/edit.html.twig', [
            'restaurant' => $restaurant,
            'form' => $form->createView()
        ]);
    }

    #[Route('/restaurant/remove/{id}/{token}', name: 'restaurant_remove', methods: ["POST"])]
    public function remove(Restaurant $restaurant, $token): Response
    {
        if ($this->isCsrfTokenValid('remove_restaurant', $token)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($restaurant);
            $em->flush();

            $this->addFlash('success', "Le restaurant {$restaurant->getName()} à bien été supprimé.");

            return $this->redirectToRoute('restaurant_list');
        }

        throw new Exception('Invalid token CSRF');
    }

    #[Route('/restaurant/{id}', name: 'restaurant_show')]
    public function show(Restaurant $restaurant): Response
    {
        $restaurant_info = [
            'id' => $restaurant->getId(),
            'nom' => $restaurant->getName(),
            'activité' => $restaurant->getActivity(),
            'date de création' => $restaurant->getCreatedAt(),
        ];

        $restaurant_social_network = [
            'facebook_url' => $restaurant->getFacebookUrl(),
            'instagram_url' => $restaurant->getInstagramUrl(),
            'twitter_url' => $restaurant->getTwitterUrl(),
        ];

        return $this->render('admin/restaurant/show.html.twig', [
            'restaurant' => $restaurant,
            'restaurant_info' => $restaurant_info,
            'restaurant_social_network' => $restaurant_social_network,
        ]);
    }

}

<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\CreateUserType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use JMS\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function index(): Response
    {
        return $this->render('security/index.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }

    #[Route('/login', name: 'login', methods: ['GET','POST'])]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            if (in_array("ROLE_ADMIN", $this->getUser()->getRoles(), true)) {
                return $this->redirectToRoute('admin_home_default');
            } else if (in_array("ROLE_SUPER_ADMIN", $this->getUser()->getRoles(), true)) {
                return $this->redirectToRoute('admin_home_default');
            } else {
                return $this->redirectToRoute('admin_logout');
            }
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    #[Route('/logout', name: 'logout', methods: ['GET'])]
    public function logout(): void
    {}
}

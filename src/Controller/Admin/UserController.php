<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use App\Form\UserSuperAdminType;
use App\Repository\MenuRepository;
use App\Repository\UserRepository;
use App\Service\DashboardConfigService;
use Carbon\CarbonImmutable;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[Route('/users')]
class UserController extends AbstractController
{
    const CONTROLLER_NAME = 'Utilisateur';

    const DASHBOARD_FILTER_SELECTION = [
        [
            "name" => UserRepository::FILTER_NAME,
            "value" => UserRepository::FILTER_VALUE
        ],
    ];

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    #[Route('/dashboard/{chart_selected}/{chart_filter}/{years}/{months}/{days}/{hours}', name: 'user_dashboard', defaults: [ "chart_selected" => self::DASHBOARD_FILTER_SELECTION[0]["value"], "chart_filter" => DashboardConfigService::MINUTES, "years" => "", "months" => "", "days" => "", "hours" => "" ], methods: ['GET'])]
    public function dashboard(string $chart_selected, string $chart_filter, string $years, string $months, string $days, string $hours, ChartBuilderInterface $chartBuilder, UserRepository $userRepository, EntityManagerInterface $em): Response
    {
        $now = CarbonImmutable::now();
        $years = $years?intval($years):$now->year;
        $months = $months?intval($months):$now->month;
        $days = $days?intval($days):$now->day;
        $hours = $hours?intval($hours):$now->hour;

        $chart = $chartBuilder->createChart(Chart::TYPE_BAR);
        $dashboard_config_service = New DashboardConfigService($em, $chart, $chart_selected, $chart_filter);
        $dashboard_config_service->addSelectionRange(self::DASHBOARD_FILTER_SELECTION);
        $dashboard_config_service->setChart($years, $months, $days, $hours);
        $bottom_value = [
            'Utilisateur' => $userRepository->usersCount(),
        ];

        $dashboard_config_service->addBottomSelectionRange($bottom_value);

        return $this->render('admin/user/dashboard.html.twig', [
            'bottom_value' => $dashboard_config_service->getBottomSelection(),
            'years' => $years,
            'months' => $months,
            'days' => $days,
            'hours' => $hours,
            'chart' => $dashboard_config_service->getChart(),
            'chart_filter' => $chart_filter,
            'chart_selected' => $chart_selected,
            'chart_selection' => $dashboard_config_service->getSelection(),
            'chart_filter_parameter' => $dashboard_config_service->getFilter(),
            'latest_value' => $dashboard_config_service->getLatestValue(),
        ]);
    }

    #[Route('/list', name: 'user_list', methods: ['GET'])]
    public function list(Request $request, UserRepository $userRepository): Response
    {
        $entities = [];

        if ( $request->query->all() ) {
            $entities = $userRepository->findAllToArrayByParameters($request->query->all());
        } else {
            $entities = $userRepository->findAllToArray();
        }

        $classMetaData = $this->getDoctrine()->getManager()->getClassMetadata($userRepository->getClassName());
        $filterParameter = $classMetaData->getFieldNames();

        $filteredValues = [];

        foreach ($userRepository->findAllToArray() as $user) {
            if (!$filteredValues) {
                $filteredValues = $user;
            } else {
                $filteredValues = array_merge_recursive($filteredValues, $user);
            }
        }

        foreach ($filteredValues as $key => $value) {
            if (is_array($value)) {
                $filteredValues[$key] = array_unique($value, SORT_REGULAR);
            } else {
                $filteredValues[$key] = $value;
            }
        }

        return $this->render('admin/user/list.html.twig', [
            'entities' => $entities,
            'filterParameters' => $filterParameter,
            'filterValues' => $filteredValues,
            'entityName' => $classMetaData->table['name'],
        ]);
    }

    #[Route('/new', name: 'user_create', methods: ['GET','POST'])]
    public function new(Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $user = new User();

        if(in_array("ROLE_SUPER_ADMIN", $this->getUser()->getRoles(), true)){
            $form = $this->createForm(UserSuperAdminType::class, $user);
        } else {
            $form = $this->createForm(UserType::class, $user);
        }
        // Generate a random password and send an email
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (in_array("user", $request->request->all())) {
                if(gettype($request->request->all()['user']['Roles']) === "string") {
                    $user->setRoles([$request->request->all()['user']['Roles']]);
                }
            } else if (in_array("user_super_admin", $request->request->all())) {
                if(gettype($request->request->all()['user_super_admin']['Roles']) === "string") {
                    $user->setRoles([$request->request->all()['user_super_admin']['Roles']]);
                }
            }

            $comb = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            $tmp_password = substr(str_shuffle($comb),0,15);

            $user->setPassword($passwordHasher->hashPassword($user, $tmp_password));

            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash('success', 'Utilisateur ajouté avec succès!');
                return $this->redirectToRoute('admin_user_create', [], Response::HTTP_CREATED);
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash('failure', 'Une erreur est survenue: '.$e->getMessage());
                return $this->redirectToRoute('admin_user_create', [], Response::HTTP_BAD_REQUEST);

                //return $this->redirectToRoute('app_create_user', [], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->renderForm('admin/user/create_user.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/edit/{id}', name: 'user_edit', methods: ['GET','POST'])]
    public function edit(User $user, Request $request): Response
    {
        if (in_array("ROLE_SUPER_ADMIN", $this->getUser()->getRoles(), true)) {
            $form = $this->createForm(UserSuperAdminType::class, $user);
        } else {
            if (in_array("ROLE_SUPER_ADMIN", $user->getRoles(), true)) {
                return $this->redirectToRoute('admin_user_show', [
                    'id' => $user->getId()
                ]);
            } else if (in_array("ROLE_ADMIN", $user->getRoles(), true) && $this->getUser()->getId() !== $user->getId()) {
                return $this->redirectToRoute('admin_user_show', [
                    'id' => $user->getId()
                ]);
            } else {
                $form = $this->createForm(UserType::class, $user);
            }
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('green', "Le lieu {$user->getEmail()} à bien été édité.");

            return $this->redirectToRoute('admin_user_edit', [
                'id' => $user->getId()
            ]);
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    #[Route('/remove/{id}/{token}', name: 'user_remove')]
    public function remove(User $user, $token): Response
    {
        if (!in_array("ROLE_SUPER_ADMIN", $this->getUser()->getRoles(), true)) {
            if (in_array("ROLE_SUPER_ADMIN", $user->getRoles(), true) || in_array("ROLE_ADMIN", $user->getRoles(), true)) {
                $this->addFlash('failure', 'Vous ne pouvez pas supprimez cet utilisateur (' . $user->getEmail() . ')');
                return $this->redirectToRoute('admin_user_default');
            }
        }

        if ($this->isCsrfTokenValid('remove_user', $token)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            $this->addFlash('success', "L'utilisateur {$user->getEmail()} à bien été supprimé.");

            return $this->redirectToRoute('admin_user_default');
        }

        throw new Exception('Invalid token CSRF');
    }


    #[Route('/{id}', name: 'user_show')]
    public function show(User $user): Response
    {
        $user_info = [];

        $user_info = [
            'id' => $user->getId(),
            'prénom' => $user->getFirstname(),
            'nom de famille' => $user->getLastname(),
            'rôle' => $user->getRoles(),
            'téléphone' => $user->getPhone(),
            'date de naissance' => $user->getBirthday(),
            'profil visible' => $user->getIsShown(),
            'date de création' => $user->getCreatedAt(),
        ];

        $user_social_network = [
            'facebook_url' => $user->getFacebookUrl(),
            'instagram_url' => $user->getInstagramUrl(),
            'twitter_url' => $user->getTwitterUrl(),
        ];

        return $this->render('admin/user/show.html.twig', [
            'user' => $user,
            'user_info' => $user_info,
            'user_social_network' => $user_social_network,
        ]);
    }
}

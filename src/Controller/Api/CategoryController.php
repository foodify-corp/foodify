<?php

namespace App\Controller\Api;

use App\Entity\Categories;
use App\Entity\CategoriesDishes;
use App\Entity\Restaurant;
use App\Repository\CategoriesRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant')]
class CategoryController extends AbstractController
{
    #[Route('/{id}/category', name: 'show_category', methods: ['GET'])]
    public function categories(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository
    ): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $categories = $restaurant->getCategories();
        $response = array("categories" => $categories);

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["category", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" =>  "application/json"]);
    }

    #[Route('/{id}/category', name: 'add_category', methods: ['POST'])]
    #[IsGranted("ROLE_MANAGER")]
    public function addCategory(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        Request $request,
        RestaurantRepository $restaurantRepository,
        UserRepository $userRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $restaurant = $restaurantRepository->find($restaurant);
        $userRestaurant = $user->getRestaurants();

        if($userRestaurant->contains($restaurant)) {
            $categories = $restaurant->getCategories();
            $payload = json_decode($request->getContent(), true);

            $name = strtolower($payload['name']);
            $categoryExist = $categories->exists(function ($key, $element) use ($name) {
                return strtolower($element->getName()) === strtolower($name);
            });

            $category = new Categories();

            if (!$categoryExist) {
                $category->setName($payload['name']);
                $category->setRestaurantId($restaurant);
                $categories->add($category);
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->persist($restaurant);

                $categoryDish = new CategoriesDishes();
                $categoryDish->setCategory($category);
                $em->persist($categoryDish);
                $em->flush();
            } else {
                $error = array("message" => "La catégorie '$name' existe déjà");
                return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
            }
            $response = array("category" => $category, "message" => "Catégorie créée");

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["category", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ["Content-Type" =>  "application/json"]);
        }

        $error = array("message" => "Erreur lors de la création de la catégorie");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/category/{categoryId}', name: 'update_category', methods: ['PUT'])]
    #[IsGranted("ROLE_MANAGER")]
    public function updateCategory(
        SerializerInterface $serializer,
        Request $request,
        String $categoryId,
        CategoriesRepository $categoriesRepository,
        RestaurantRepository $restaurantRepository,
        UserRepository $userRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $userRestaurant = $user->getRestaurants();
        $category = $categoriesRepository->find($categoryId);
        $payload = json_decode($request->getContent(), true);
        $restaurant = $restaurantRepository->find($category->getRestaurantId());
        $categories = $restaurant->getCategories();
        $name = strtolower($payload['name']);

        if($userRestaurant->contains($restaurant)) {
            $categoryExist = $categories->exists(function ($key, $element) use($name){
                return strtolower($element->getName()) == $name;
            });
            if(!$categoryExist) {
                $em = $this->getDoctrine()->getManager();
                $category->setName($payload['name']);
                $em->flush();

                $response = array("category" => $category, "message" => "Catégorie mise à jour");

                $jsonObject = $serializer->serialize($response, 'json', [
                    'groups' => ["category", "timestamp"],
                    'circular_reference_handler' => function ($object) {
                        return $object->getId();
                    }
                ]);

                return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
            }
            $error = array("message" => "La catégorie '$name' existe déjà");
            return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);

        }
        $response = array("message" => "Erreur lors de la mise à jour de la catégorie");
        return new Response(json_encode($response), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/category/{categoryId}', name: 'remove_category', methods: ['DELETE'])]
    #[IsGranted("ROLE_MANAGER")]
    public function removeCategory(
        String $categoryId,
        CategoriesRepository $categoriesRepository,
        UserRepository $userRepository,
        RestaurantRepository $restaurantRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $userRestaurant = $user->getRestaurants();
        $category = $categoriesRepository->find($categoryId);
        $restaurant = $restaurantRepository->find($category->getRestaurantId());
        $categories = $restaurant->getCategories();

        if($userRestaurant->contains($restaurant) and $categories->contains($category)) {
            $em = $this->getDoctrine()->getManager();
            $deletedCategoryId = $category->getId();
            $em->remove($category);
            $em->flush();
            $response = array("deleted_category_id" => $deletedCategoryId, 'message' => "Catégorie supprimée");

            return new Response(json_encode($response), 202, ['Content-Type' => 'application/json']);
        }
        $error = array("message" => "Erreur lors de la suppression de la catégorie");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }
}
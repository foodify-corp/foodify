<?php

namespace App\Controller\Api;

use App\Entity\Comment;
use App\Entity\Restaurant;
use App\Entity\RestaurantComments;
use App\Repository\UserRepository;
use App\Repository\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant')]
class CommentController extends AbstractController
{
    #[Route('/{id}/comment', name: 'add_comment', methods: ['POST'])]
    public function addComment(
        SerializerInterface $serializer,
        Request $request,
        RestaurantRepository $restaurantRepository,
        UserRepository $userRepository,
        String $id
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $restaurant = $restaurantRepository->find($id);

        $data = $request->getContent();
        $comment = $serializer->deserialize($data, Comment::class, 'json');
        $comment->setUserId($user);
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);

        $restaurantComments = new RestaurantComments();
        $restaurantComments->addComment($comment);
        $restaurantComments->setRestaurant($restaurant);

        $em->persist($restaurantComments);
        $em->flush();

        $response = array("comment" => $comment);
        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["comment", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" =>  "application/json"]);
    }

    #[Route('/{id}/comment', name: 'show_comments', methods: ['GET'])]
    public function comments(
        SerializerInterface $serializer,
        RestaurantRepository $restaurantRepository,
        String $id
    ): Response
    {
        $restaurant = $restaurantRepository->find($id);
        $comments = $restaurant->getRestaurantComments();

        $response = array("comments" => $comments);
        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["restaurant_comment", "comment", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" =>  "application/json"]);
    }
}
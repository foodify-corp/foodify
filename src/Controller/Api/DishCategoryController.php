<?php

namespace App\Controller\Api;

use App\Entity\Restaurant;
use App\Repository\CategoriesRepository;
use App\Repository\DishRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant')]
class DishCategoryController extends AbstractController
{
    #[Route('/{id}/category-dish', name: "show_all_category_dishes", methods: ['GET'])]
    function allCategoryDishes(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository,
    ): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);

        $categories = $restaurant->getCategories();
        $allCategories = [];

        for($i = 0; $i < $categories->count(); $i ++) {
            array_push($allCategories, $categories[$i]->getCategoriesDishes());
        }

        if (sizeof($allCategories) > 0) {
            $response = array("categories_dishes" => $allCategories);
            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["category_dishes", "dish", "category", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);
            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }

        $error = array("message" => "Erreur lors de la récupération du menu");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/category/{categoryId}/category-dish', name: "show_category_dishes", methods: ['GET'])]
    function categoryDishes(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository,
        CategoriesRepository $categoriesRepository,
        String $categoryId
    ): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $category = $categoriesRepository->find($categoryId);

        $categories = $restaurant->getCategories();
        if($categories->contains($category)) {
            $response = array("category_dishes" => $category->getCategoriesDishes());

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["category_dishes", "dish", "category", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);
            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }
        $error = array("message" => "Aucune catégories trouvée pour ce restaurant");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/category/{categoryId}/category-dish', name: "add_category_dishes", methods: ['POST'])]
    #[isGranted('ROLE_MANAGER')]
    function addCategoryDish(
        SerializerInterface $serializer,
        UserRepository $userRepository,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository,
        CategoriesRepository $categoriesRepository,
        String $categoryId,
        DishRepository $dishRepository,
        Request $request
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $restaurant = $restaurantRepository->find($restaurant);
        $payload = json_decode($request->getContent(), true);
        $dishesId = $payload['dishes_id'];
        $categories = $restaurant->getCategories();
        $category = $categoriesRepository->find($categoryId);

        if($user->getRestaurants()->contains($restaurant)) {
            if($categories->contains($category)) {

                $dishCategory = $category->getCategoriesDishes();
                $addedDish = [];

                for($i = 0; $i < count($dishesId); $i ++) {
                    $dish = $dishRepository->find($dishesId[$i]);
                    $dishCategory->addDishes($dish);
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                    array_push($addedDish, $dish);
                }

                $response = array("category_dishes" => $addedDish, "message" => "Plat ajouté");

                $jsonObject = $serializer->serialize($response, 'json', [
                    'groups' => ["category_dishes", "dish", "category", "timestamp"],
                    'circular_reference_handler' => function ($object) {
                        return $object->getId();
                    }
                ]);

                return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
            }
            $error = array("message" => "Erreur lors de l'ajout du plat");
            return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
        }
        $error = array("message" => "Erreur lors de l'ajout du plat");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/category/{categoryId}/category-dish/{dishId}', name: "remove_category_dishes", methods: ['DELETE'])]
    #[IsGranted('ROLE_MANAGER')]
    function deleteMenuDish(
        SerializerInterface $serializer,
        UserRepository $userRepository,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository,
        CategoriesRepository $categoriesRepository,
        String $categoryId,
        String $dishId,
        DishRepository $dishRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $restaurant = $restaurantRepository->find($restaurant);
        $categories = $restaurant->getCategories();
        $category = $categoriesRepository->find($categoryId);
        $dish = $dishRepository->find($dishId);

        if($user->getRestaurants()->contains($restaurant)) {
            if($categories->contains($category) && $category->getCategoriesDishes()->getDishes()->contains($dish)) {

                $deletedDishId = $dish->getId();
                $categoryDish = $category->getCategoriesDishes();
                $categoryDish->removeDish($dish);
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                $response = array("deleted_dish_id" => $deletedDishId, "message" => "Plat supprimé de la catégorie");

                return new Response(json_encode($response), 200, ["Content-Type" => "application/json"]);
            }
            $error = array("message" => "Erreur lors de la suppresion du plat");
            return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
        }
        $error = array("message" => "Erreur lors de la suppresion du plat");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }
}
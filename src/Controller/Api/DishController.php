<?php

namespace App\Controller\Api;

use App\Entity\Dish;
use App\Entity\Restaurant;
use App\Repository\DishRepository;
use App\Repository\MenuRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant')]
class DishController extends AbstractController
{
    #[Route('/{id}/dish', name: 'show_dish', methods: ['GET'])]
    public function dish(SerializerInterface $serializer, Restaurant $restaurant, RestaurantRepository $restaurantRepository): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $dishes = $restaurant->getDishes();
        $response = array("dishes" => $dishes);

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["dish", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" =>  "application/json"]);
    }

    #[Route('/{id}/dish', name: 'add_dish', methods: ['POST'])]
    #[IsGranted("ROLE_MANAGER")]
    public function addDish(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        Request $request,
        RestaurantRepository $restaurantRepository,
        UserRepository $userRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $userRestaurants = $user->getRestaurants();
        $restaurant = $restaurantRepository->find($restaurant);
        if($userRestaurants->contains($restaurant)) {
            $data = $request->getContent();
            $dish = $serializer->deserialize($data, Dish::class, 'json');
            $restaurant->addDish($dish);
            $em = $this->getDoctrine()->getManager();
            $em->persist($dish);
            $em->flush();

            $response = array("dish" => $dish, "message" => "Plat ajouté");

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["dish", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ["Content-Type" =>  "application/json"]);
        }
        $error = array("message" => "Vous ne possédez pas ce restaurant");
        return new Response(json_encode($error), 400, ["Content-Type" =>  "application/json"]);
    }

    #[Route('/{id}/dish/{dishId}', name: 'update_dish', methods: ['PUT'])]
    #[IsGranted("ROLE_MANAGER")]
    public function updateDish(
        SerializerInterface $serializer,
        Request $request,
        String $id,
        String $dishId,
        DishRepository $dishRepository,
        UserRepository $userRepository,
        RestaurantRepository $restaurantRepository,
        MenuRepository $menuRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $userRestaurants = $user->getRestaurants();
        $dish = $dishRepository->find($dishId);
        $restaurant = $restaurantRepository->find($dish->getRestaurantId());
        $data = json_decode($request->getContent(), true);

        if($userRestaurants->contains($restaurant)) {
            $em = $this->getDoctrine()->getManager();
            $dish->setName($data['name']);
            $dish->setPrice($data['price']);
            $dish->setIsAvailable($data['is_available']);
            $dish->setIsAvailableTime($data['is_available_time']);
//            if($data['menu_id']) {
//                $menuDish = new MenuDishes();
//                $menuDish->addDishes($dish);
//                $menu = $menuRepository->find($data['menu_id']);
//                $dish->addMenuDish($menu);
//            }
            $em->flush();

            $response = array("dish" => $dish, "message" => "Plat modifié");

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["dish", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);
            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }
        $error = array("message" => "Ce plat n'existe pas");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/dish/{dishId}', name: 'remove_dish', methods: ['DELETE'])]
    #[IsGranted("ROLE_MANAGER")]
    public function removeDish(
        DishRepository $dishRepository,
        String $dishId,
        RestaurantRepository $restaurantRepository,
        UserRepository $userRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $userRestaurants = $user->getRestaurants();
        $dish = $dishRepository->find($dishId);
        $restaurant = $restaurantRepository->find($dish->getRestaurantId());
        if($userRestaurants->contains($restaurant)) {
            $deletedDishId = $dish->getId();
            $em = $this->getDoctrine()->getManager();
            $em->remove($dish);
            $em->flush();

            $response = array("deleted_dish_id" => $deletedDishId, "message" => "Plat supprimé");

            return new Response(json_encode($response), 202, ['Content-Type' => 'application/json']);
        }
        $error = array("message" => "Ce plat n'existe pas");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }
}

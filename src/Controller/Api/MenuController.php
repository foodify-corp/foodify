<?php

namespace App\Controller\Api;

use App\Entity\Menu;
use App\Entity\MenuDishes;
use App\Entity\Restaurant;
use App\Repository\MenuRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant')]
class MenuController extends AbstractController
{
    #[Route('/{id}/menu/{menuId}', name: 'show_menu', methods: ['GET'])]
    public function menu(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository,
        String $menuId,
        MenuRepository $menuRepository
    ): Response
    {
        $menu = $menuRepository->find($menuId);
        $restaurantMenus = $restaurantRepository->find($restaurant)->getMenus();

        if($restaurantMenus->contains($menu)) {
            $response = array("menu" => $menu);

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["menu", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);
            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }

        $error = array("message" => "Le menu n'a pu être récupéré");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/menu', name: 'show_menus', methods: ['GET'])]
    public function menus(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository
    ): Response
    {
        $restaurantMenus = $restaurantRepository->find($restaurant)->getMenus();

        $response = array("menus" => $restaurantMenus);

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["menu", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/menu', name: 'add_menu', methods: ['POST'])]
    public function addMenu(
        SerializerInterface $serializer,
        Request $request,
        RestaurantRepository $restaurantRepository,
        Restaurant $restaurant,
        UserRepository $userRepository
    ): Response
    {
        $userRestaurants = $userRepository->find($this->getUser()->getId())->getRestaurants();
        $restaurant = $restaurantRepository->find($restaurant);

        $data = $request->getContent();
        if($userRestaurants->contains($restaurant)) {

            $menu = $serializer->deserialize($data, Menu::class, 'json');
            $restaurant->addMenu($menu);
            $em = $this->getDoctrine()->getManager();
            $em->persist($menu);

            $menuDish = new MenuDishes();
            $menuDish->setMenu($menu);
            $em->persist($menuDish);
            $em->flush();

            $response = array("menu" => $menu, "message" => "Menu ajouté");

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["menu", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }

        $error = array("message" => "Le menu n'a pas pu être sauvegardé");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/menu/{menuId}', name: 'update_menu', methods: ['PUT'])]
    public function updateMenu(
        SerializerInterface $serializer,
        Request $request,
        String $menuId,
        MenuRepository $menuRepository,
        RestaurantRepository $restaurantRepository,
        Restaurant $restaurant,
        UserRepository $userRepository
    ): Response
    {
        $userRestaurants = $userRepository->find($this->getUser()->getId())->getRestaurants();
        $restaurant = $restaurantRepository->find($restaurant);
        $menu = $menuRepository->find($menuId);

        if($userRestaurants->contains($restaurant) && $restaurant->getMenus()->contains($menu)) {
            $payload = json_decode($request->getContent(), true);
            $menu->setName($payload['name']);
            $menu->setDisplay($payload['display']);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $response = array("menu" => $menu, "message" => "Menu mis à jour !");

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["menu", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }
        $error = array("message" => "Le menu n'a pas pu être mis à jour");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/menu/{menuId}', name: 'remove_menu', methods: ['DELETE'])]
    public function removeMenu(
        SerializerInterface $serializer,
        String $menuId,
        MenuRepository $menuRepository,
        RestaurantRepository $restaurantRepository,
        Restaurant $restaurant,
        UserRepository $userRepository
    ): Response
    {
        $userRestaurants = $userRepository->find($this->getUser()->getId())->getRestaurants();
        $restaurant = $restaurantRepository->find($restaurant);
        $menu = $menuRepository->find($menuId);

        if($userRestaurants->contains($restaurant) && $restaurant->getMenus()->contains($menu)) {
            $deletedMenuId = $menu->getId();
            $em = $this->getDoctrine()->getManager();
            $em->remove($menu);
            $em->flush();
            $response = array("deleted_menu_id" => $deletedMenuId, "message" => "Menu supprimé !");

            return new Response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
        $error = array("message" => "Erreur lors de la suppression du menu");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }
}
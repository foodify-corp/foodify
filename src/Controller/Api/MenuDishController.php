<?php

namespace App\Controller\Api;

use App\Entity\Restaurant;
use App\Repository\DishRepository;
use App\Repository\MenuRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant')]
class MenuDishController extends AbstractController
{
    #[Route('/{id}/menu/{menuId}/menu-dish', name: "show_menu_dishes", methods: ['GET'])]
    function menuDishes(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository,
        MenuRepository $menuRepository,
        String $menuId
    ): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $menu = $menuRepository->find($menuId);

        $menus = $restaurant->getMenus();
        if($menus->contains($menu)) {
            $response = array("menu_dishes" => $menu->getMenuDishes());

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["menu_dish", "menu", "dish", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);
            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }
        $error = array("error" => "Erreur lors de la récupération du menu");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/menu-dishes', name: "show_all_menu_dishes", methods: ['GET'])]
    function allMenuDishes(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository,
    ): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $menus = $restaurant->getMenus();
        if(count($menus) !== 0) {
            $menuDishes = array();

            foreach ($menus as $menu) {
                $menuDishes[] = $menu->getMenuDishes();
            }

            $response = array("menu_dishes" => $menuDishes);

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["menu_dish", "menu", "dish", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);
            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        } else {
            $error = array("error" => "Ce restaurant ne possède aucun menu");
            return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
        }
    }

    #[Route('/{id}/menu/{menuId}/menu-dish', name: "add_menu_dish", methods: ['POST'])]
    #[isGranted('ROLE_MANAGER')]
    function addMenuDish(
        SerializerInterface $serializer,
        UserRepository $userRepository,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository,
        MenuRepository $menuRepository,
        String $menuId,
        DishRepository $dishRepository,
        Request $request
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $restaurant = $restaurantRepository->find($restaurant);
        $payload = json_decode($request->getContent(), true);
        $dishesId = $payload['dishes_id'];
        $menus = $restaurant->getMenus();
        $menu = $menuRepository->find($menuId);

        if($user->getRestaurants()->contains($restaurant)) {
            if($menus->contains($menu)) {

                $menuDish = $menu->getMenuDishes();
                $addedDish = [];

                for($i = 0; $i < count($dishesId); $i ++) {
                    $dish = $dishRepository->find($dishesId[$i]);
                    $menuDish->addDishes($dish);
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                    array_push($addedDish, $dish);
                }

                $response = array("menu_dishes" => $addedDish, "message" => "Plat ajouté au menu");

                $jsonObject = $serializer->serialize($response, 'json', [
                    'groups' => ["menu_dish", "menu", "dish", "timestamp"],
                    'circular_reference_handler' => function ($object) {
                        return $object->getId();
                    }
                ]);

                return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
            }
            $error = array("error" => "Erreur lors de l'ajout du plat au menu");
            return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
        }
        $error = array("error" => "Erreur lors de l'ajout du plat au menu");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/menu/{menuId}/menu-dish/{dishId}', name: "remove_menu_dish", methods: ['DELETE'])]
    #[IsGranted('ROLE_MANAGER')]
    function deleteMenuDish(
        UserRepository $userRepository,
        Restaurant $restaurant,
        RestaurantRepository $restaurantRepository,
        MenuRepository $menuRepository,
        String $menuId,
        String $dishId,
        DishRepository $dishRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $restaurant = $restaurantRepository->find($restaurant);
        $menus = $restaurant->getMenus();
        $menu = $menuRepository->find($menuId);
        $dish = $dishRepository->find($dishId);

        if($user->getRestaurants()->contains($restaurant)) {
            if($menus->contains($menu) && $menu->getMenuDishes()->getDishes()->contains($dish)) {

                $deletedDishId = $dish->getId();
                $menuDish = $menu->getMenuDishes();
                $menuDish->removeDish($dish);
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                $response = array("deleted_dish_id" => $deletedDishId, "message" => "Plat supprimé du menu");
                return new Response(json_encode($response), 200, ["Content-Type" => "application/json"]);
            }
            $error = array("error" => "Erreur lors de la suppresion du plat");
            return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
        }
        $error = array("error" => "Erreur lors de la suppresion du plat");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }
}
<?php

namespace App\Controller\Api;

use App\Entity\Order;
use App\Entity\OrderDish;
use App\Entity\Restaurant;
use App\Entity\User;
use App\Form\OrderType;
use App\Repository\DishRepository;
use App\Repository\OrderDishRepository;
use App\Repository\OrderRepository;
use App\Repository\UserRepository;
use App\Repository\UserWorkAtRestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant/{id}/orders')]
class OrderController extends AbstractController
{
    #[Route('', name: 'order_new', methods: ['POST'])]
    public function new(Request $request, Restaurant $restaurant, UserRepository $userRepository, UserWorkAtRestaurantRepository $userWorkAtRestaurantRepository, DishRepository $dishRepository, SerializerInterface $serializer): Response
    {
        $order = $serializer->deserialize($request->getContent(), Order::class, 'json');

        // Add missing properties to Order entity
        $user = $userRepository->find($this->getUser()->getId());
        $order->setUser($user);
        $order->setRestaurant($restaurant);
        $order->setStatus($order::STATUS_PENDING);

        // Set Order dishes
        $order_dishes = $order->getOrderDishes();
        foreach($order_dishes as $order_dish){
            $dishName = $order_dish->getDish()->getName();
            $quantity = $order_dish->getQuantity();

            $dish = $dishRepository->findOneByName($dishName);
            if (!$dish) {
                $error = array("message" => "Le plat " . $dishName . " n'existe pas dans ce restaurant");
                return new Response(json_encode($error), 400, ['Content-Type' => 'application/json']);
            }

            if ($quantity <= 0) {
                $error = array("message" => "La quantité de " . $dishName . " doit être supérieur à 0");
                return new Response(json_encode($error), 400, ['Content-Type' => 'application/json']);
            }

            $order_dish->setDish($dish);
            $order_dish->SetOrder($order);
            $order->addOrderDish($order_dish);
        }

        // Save to DB
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        $jsonObject = $serializer->serialize($order, 'json', [
            'groups' => ['get_orders', 'get_order_dish', 'get_dish', 'get_dish_type', 'timestamp', 'dish'],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
    }

    #[Route('/status/{status}', name: 'get_todays_orders', methods: ['GET'])]
    public function getTodaysOrder(Request $request, Restaurant $restaurant, OrderRepository $orderRepository, SerializerInterface $serializer) {
        $status = ucfirst($request->attributes->get('status'));
        $todaysOrders = $orderRepository->findTodaysOrdersByRestaurantAndStatus($restaurant, $status);
        $todaysOrdersArray = array('orders' => $todaysOrders);

        if($todaysOrders) {
            $jsonObject = $serializer->serialize($todaysOrdersArray, 'json', [
                'groups' => ["get_orders", "get_dish", "dish"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
        }

        return new Response(json_encode([]));
    }

    #[Route('/{orderId}', name: 'update_order', methods: ['PUT'])]
    public function updateOrderStatus(Request $request, Restaurant $restaurant, string $orderId, OrderRepository $orderRepository, DishRepository $dishRepository, UserRepository $userRepository, OrderDishRepository $orderDishRepository, UserWorkAtRestaurantRepository $userWorkAtRestaurantRepository, SerializerInterface $serializer) {
        $payload = json_decode($request->getContent(), true);
        $user = $userRepository->find($this->getUser()->getId());
        $order = $orderRepository->find($orderId);

        // Check if the user work at the restaurant
        $userWorkAtRestaurant = $userWorkAtRestaurantRepository->findUserWorkAtRestaurant($user, $restaurant);
        // if not, return 400 + error msg
        if ($userWorkAtRestaurant === null) {
            $errorMsg = array("message" => "L'utilisateur [" . $user->getId() . "] ne peut pas mettre à jour le status de la commande [" . $orderId . "] car il ne travaille pas dans le restaurant [" . $restaurant->getId() . "]");
            return new Response(json_encode($errorMsg), 400, ['Content-Type' => 'application/json']);
        }
        // if yes, update order
        $order->setStatus($payload['status']);
        $order->setOrderType($payload['order_type']);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $response = array("order" => $order);
        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ['get_orders', 'get_order_dish', 'get_dish', 'get_dish_type', 'timestamp', 'dish'],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
    }

}

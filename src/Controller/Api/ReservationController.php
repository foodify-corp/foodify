<?php

namespace App\Controller\Api;

use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use App\Repository\RestaurantRepository;
use App\Repository\TableRepository;
use App\Repository\UserRepository;
use Carbon\CarbonImmutable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant')]
class ReservationController extends AbstractController
{
    #[Route('/{id}/reservation', name: 'show_reservation', methods: ['GET'])]
    public function showReservation(SerializerInterface $serializer, String $id, RestaurantRepository $restaurantRepository): Response
    {
        $restaurant = $restaurantRepository->find($id);
        $reservations = $restaurant->getReservations();

        $response = array("reservations" => $reservations);

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["reservation", "timestamp", "table"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/reservation', name: 'add_reservation', methods: ['POST'])]
    public function addReservation(
        UserRepository $userRepository,
        SerializerInterface $serializer,
        RestaurantRepository $restaurantRepository,
        String $id,
        Request $request
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $restaurant = $restaurantRepository->find($id);

        $reservation = $serializer->deserialize($request->getContent(), Reservation::class, 'json');

        $restaurant->addReservation($reservation);
        $user->addReservation($reservation);

        $em = $this->getDoctrine()->getManager();
        $em->persist($reservation);
        $em->flush();

        $response = array("reservation" => $reservation, "message" => "Réservation effectuée avec succès");

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["reservation", "timestamp", "table"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/reservation/{idReservation}', name: 'reservation', methods: ['PUT'])]
    #[IsGranted("ROLE_MANAGER")]
    public function reservation(
        SerializerInterface $serializer,
        String $id,
        String $idReservation,
        RestaurantRepository $restaurantRepository,
        ReservationRepository $reservationRepository,
        TableRepository $tableRepository,
        Request $request
    ): Response
    {
        $restaurant = $restaurantRepository->find($id);
        $reservations = $restaurant->getReservations();
        $reservation = $reservationRepository->find($idReservation);
        $payload = json_decode($request->getContent(), true);
        $tableId = $payload['table_id'];
        if($tableId !== null) {
            $table = $tableRepository->find($tableId);
        }

        if($reservations->contains($reservation)) {

            switch ($payload['status']) {
                case Reservation::ACCEPTED:
                    $reservation->setStatus($reservation::ACCEPTED);
                    $reservation->setIsValidated(true);
                    $reservation->setConfirmedAt(CarbonImmutable::now());
                    if($tableId !== null) {
                        $table->addReservation($reservation);
                    }
                    //TODO Envoyer email d'info concernant la resa
                break;
                case Reservation::REFUSED:
                    $reservation->setStatus($reservation::REFUSED);
                    $reservation->setIsValidated(false);
                    //TODO Envoyer email d'info concernant la resa
                break;
                case Reservation::CANCELED:
                    $reservation->setStatus($reservation::CANCELED);
                    $reservation->setIsValidated(false);
                    //TODO Envoyer email d'info concernant la resa
                    break;
                case Reservation::AVAILABLE:
                    $reservation->setStatus($reservation::AVAILABLE);
                break;
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $response = array("reservation" => $reservation);

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["reservation", "timestamp", "table"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }

        $error = array("message" => "Erreur lors de la mise à jour");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }
}

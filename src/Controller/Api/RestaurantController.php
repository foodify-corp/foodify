<?php

namespace App\Controller\Api;

use App\Encoder\MultipartDecoder;
use App\Entity\Restaurant;
use App\Entity\RestaurantTimes;
use App\Entity\User;
use App\Entity\UserWorkAtRestaurant;
use App\Form\RestaurantType;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\Exception\ExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/restaurant')]
class RestaurantController extends AbstractController
{

    #[Route('/', name: 'show_restaurants', methods: ['GET'])]
    public function restaurants(SerializerInterface $serializer, RestaurantRepository $restaurantRepository): Response
    {
        $restaurants = $restaurantRepository->findAll();
        $response = array("restaurants" => $restaurants);

        if($restaurants) {
            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["restaurant", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
        }

        return new Response(json_encode([]));
    }

    #[Route('/{id}', name: 'show_restaurant', methods: ['GET'])]
    public function restaurant(
        SerializerInterface $serializer,
        RestaurantRepository $restaurantRepository,
        Restaurant $restaurant
    ): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $response = array("restaurant" => $restaurant);

        if($restaurant) {
            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["restaurant", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);
            return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
        }

        return new Response(json_encode([]));
    }

    #[Route('/{id}/upload-file', name: 'upload_file_restaurant', methods: ['POST'])]
    #[IsGranted("ROLE_OWNER")]
    public function uploadFile(Request $request,UserRepository $userRepository,SerializerInterface $serializer, RestaurantRepository $restaurantRepository, Restaurant $restaurant, MultipartDecoder $multipartDecoder): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);

        if(is_null($restaurant->getRestaurantFile())) {
            if ( !is_null($request->files->all()) ) {
                $restaurant->setRestaurantFile($request->files->all()["imageFile"]);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($restaurant);

        $response = array("restaurant" => $restaurant, "message" => "Image enregistrée");

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["restaurant", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
    }

    #[Route('/', name: 'add_restautant', methods: ['POST'])]
    #[IsGranted("ROLE_OWNER")]
    public function addRestaurant(Request $request, UserRepository $userRepository,SerializerInterface $serializer, MultipartDecoder $multipartDecoder): Response
    {
        $data = $request->getContent();
        $restaurant = $serializer->deserialize($data, Restaurant::class, 'json');

        $user = $userRepository->find($this->getUser()->getId());
        $payload = json_decode($request->getContent(), true);

        $restaurantTimes = new RestaurantTimes();
        $times = $payload['restaurantTimes'];

        $restaurantTimes->setMondayOpenAt($times['mondayOpenAt']);
        $restaurantTimes->setMondayCloseAt($times['mondayCloseAt']);
        $restaurantTimes->setTuesdayOpenAt($times['tuesdayOpenAt']);
        $restaurantTimes->setTuesdayCloseAt($times['tuesdayCloseAt']);
        $restaurantTimes->setWednesdayOpenAt($times['wednesdayOpenAt']);
        $restaurantTimes->setWednesdayCloseAt($times['wednesdayCloseAt']);
        $restaurantTimes->setThursdayOpenAt($times['thursdayOpenAt']);
        $restaurantTimes->setThursdayCloseAt($times['thursdayCloseAt']);
        $restaurantTimes->setFridayOpenAt($times['fridayOpenAt']);
        $restaurantTimes->setFridayCloseAt($times['fridayCloseAt']);
        $restaurantTimes->setSaturdayOpenAt($times['saturdayOpenAt']);
        $restaurantTimes->setSaturdayCloseAt($times['saturdayCloseAt']);
        $restaurantTimes->setSundayOpenAt($times['sundayOpenAt']);
        $restaurantTimes->setSundayCloseAt($times['sundayCloseAt']);

        $restaurant->setRestaurantTimes($restaurantTimes);
        $user->addRestaurant($restaurant);
        $em =$this->getDoctrine()->getManager();
        $em->persist($restaurant);
        $em->flush();

        $response = array("restaurant" => $restaurant, "message" => "Restaurant créé avec succès !");

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["restaurant", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 201, ['Content-Type' => 'application/json']);
    }

    #[Route('/{id}', name: 'update_restaurant', methods: ['PUT'])]
    #[IsGranted("ROLE_OWNER")]
    public function updateRestaurant(
        UserRepository $userRepository,
        RestaurantRepository $restaurantRepository,
        String $id,
        Request $request,
        SerializerInterface $serializer
    ): Response
    {
        $payload = json_decode($request->getContent(), true);
        $user = $userRepository->find($this->getUser()->getId());
        $restaurant = $restaurantRepository->find($id);

        if($user->getRestaurants()->contains($restaurant)) {
            $restaurant->setName($payload['name']);
            $restaurant->setAddress($payload['address']);
            $restaurant->setCity($payload['city']);
            $restaurant->setCp($payload['cp']);
            $restaurant->setCountry($payload['country']);
            $restaurant->setActivity($payload['activity']);
            $restaurant->setNSiren($payload['n_siren']);
            $restaurant->setKitchenStyle($payload['kitchen_style']);
            $restaurant->setTakeAway($payload['take_away']);
            $restaurant->setOnPlace($payload['on_place']);
            $restaurant->setInstagramUrl($payload['instagram_url']);
            $restaurant->setFacebookUrl($payload['facebook_url']);
            $restaurant->setTwitterUrl($payload['twitter_url']);

            $restaurantTimes = $restaurant->getRestaurantTimes();
            $times = $payload['restaurantTimes'];
            $restaurantTimes->setMondayOpenAt($times['mondayOpenAt']);
            $restaurantTimes->setMondayCloseAt($times['mondayCloseAt']);
            $restaurantTimes->setTuesdayOpenAt($times['tuesdayOpenAt']);
            $restaurantTimes->setTuesdayCloseAt($times['tuesdayCloseAt']);
            $restaurantTimes->setWednesdayOpenAt($times['wednesdayOpenAt']);
            $restaurantTimes->setWednesdayCloseAt($times['wednesdayCloseAt']);
            $restaurantTimes->setThursdayOpenAt($times['thursdayOpenAt']);
            $restaurantTimes->setThursdayCloseAt($times['thursdayCloseAt']);
            $restaurantTimes->setFridayOpenAt($times['fridayOpenAt']);
            $restaurantTimes->setFridayCloseAt($times['fridayCloseAt']);
            $restaurantTimes->setSaturdayOpenAt($times['saturdayOpenAt']);
            $restaurantTimes->setSaturdayCloseAt($times['saturdayCloseAt']);
            $restaurantTimes->setSundayOpenAt($times['sundayOpenAt']);
            $restaurantTimes->setSundayCloseAt($times['sundayCloseAt']);
            $restaurant->setRestaurantTimes($restaurantTimes);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $response = array("restaurant" => $restaurant, "message" => "Restaurant modifié avec succès !");

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["restaurant", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
        }
        $error = array("message" => "Vous ne possédez pas ce restaurant");
        return new Response(json_encode($error), 400, ['Content-Type' => 'application/json']);
    }

    #[Route('/{id}', name: 'remove_restaurant', methods: ['DELETE'])]
    #[IsGranted("ROLE_OWNER")]
    public function removeRestaurant(
        UserRepository $userRepository,
        RestaurantRepository $restaurantRepository,
        String $id
    ): Response
    {
        $restaurant = $restaurantRepository->find($id);
        $user = $userRepository->find($this->getUser()->getId());

        if($user->getRestaurants()->contains($restaurant)) {
            $user->removeRestaurant($restaurant);
            $em = $this->getDoctrine()->getManager();
            $deletedRestaurantId = $restaurant->getId();
            $em->remove($restaurant);
            $em->flush();

            $response = array("deleted_restaurant_id" => $deletedRestaurantId, "message" => "Restaurant supprimé");

            return new Response(json_encode($response), 202);
        }
        $error = array("message" => "Vous ne possédez pas ce restaurant");
        return new Response(json_encode($error), 400, ['Content-Type' => 'application/json']);
    }

    #[Route('/{id}/generate_employee', name: 'generate_employee', methods: ['POST'])]
    public function generateEmployee(Security $security, Request $request, Restaurant $restaurant, RestaurantRepository $restaurantRepository, MailerInterface $mailer, SerializerInterface $serializer, UserRepository $userRepository, UserPasswordHasherInterface $passwordHasher): Response
    {
        $currentRestaurant = $restaurantRepository->find($restaurant);
        $employee = $serializer->deserialize($request->getContent(), User::class, 'json');

        if(empty($employee->getEmail())) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $employee->getEmail(),
        ]);

        if(!$user){
            $comb = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            $tmp_password = substr(str_shuffle($comb),0,15);

            $employee->setPassword($passwordHasher->hashPassword($employee, $tmp_password));
            $employee->setIsShown(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($employee);
            $currentRestaurant->addUsersRestaurant($employee);
            $em->flush();

            $response = array("user" => $employee, 'message' => 'Employée créé !', 'password' => $tmp_password);

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["user", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            /*try {
                $email = (new TemplatedEmail())
                    ->from(new Address('noreply@foodify.com', 'Foodify Mail Bot'))
                    ->to($employee->getEmail())
                    ->subject('[Foodify] Votre compte a été générer')
                    ->htmlTemplate('admin/user/email.html.twig')
                    ->context([
                        'user_email' => $employee->getEmail(),
                        'tmp_password' => $tmp_password,
                    ]);
                $mailer->send($email);
            } catch (Exception $e) {
                return new JsonResponse(["success" => false, "error" => $e], 401, ["Content-Type" => "application/json"]);
            }*/
            return new Response($jsonObject, Response::HTTP_CREATED, ['Content-Type' => 'application/json']);
        } else {
            return new JsonResponse(['message' => 'Email déjà utilisé'], Response::HTTP_BAD_REQUEST);
        }
    }

}

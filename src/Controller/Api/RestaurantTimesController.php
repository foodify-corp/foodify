<?php

namespace App\Controller\Api;

use App\Entity\Restaurant;
use App\Entity\RestaurantTimes;
use App\Repository\RestaurantRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant')]
class RestaurantTimesController extends AbstractController
{
    #[Route('/{id}/restaurant-times', name: 'show_restaurants_times', methods: ['GET'])]
    public function restaurantTimes(SerializerInterface $serializer, Restaurant $restaurant, RestaurantRepository $restaurantRepository): Response
    {
//        $userRestaurants = $userRepository->find($this->getUser()->getId())->getRestaurants();
        $restaurant = $restaurantRepository->find($restaurant);
        $restaurantTimes = $restaurant->getRestaurantTimes();

        $response = array("restaurant_times" => $restaurantTimes);
        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["restaurant_times"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
    }


    #[Route('/{id}/restaurant-times', name: 'add_restaurants_times', methods: ['POST'])]
    public function addRestaurantTimes(SerializerInterface $serializer, Restaurant $restaurant, Request $request, RestaurantRepository $restaurantRepository): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $data = $request->getContent();
        $restaurantTimes = $serializer->deserialize($data, RestaurantTimes::class, 'json');

        $em = $this->getDoctrine()->getManager();
        $restaurant->setRestaurantTimes($restaurantTimes);
        $em->persist($restaurant);
        $em->flush();

        $response = array("restaurant_times" => $restaurantTimes);
        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["restaurant_times"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/restaurant-times', name: 'update_restaurants_times', methods: ['PUT'])]
    public function updateRestaurantTimes(SerializerInterface $serializer, Restaurant $restaurant, Request $request, RestaurantRepository $restaurantRepository): Response
    {
//        $userRestaurants = $userRepository->find($this->getUser()->getId())->getRestaurants();
        $restaurant = $restaurantRepository->find($restaurant);
        $data = $request->getContent();

        $restaurantTimes = $serializer->deserialize($data, RestaurantTimes::class, 'json');
        $restaurantTimes->setRestaurant($restaurant);

        $em = $this->getDoctrine()->getManager();
        $restaurant->setRestaurantTimes($restaurantTimes);
        $em->persist($restaurant);

        $response = array("restaurant_times" => $restaurantTimes);
        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["restaurant_times"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
    }
}
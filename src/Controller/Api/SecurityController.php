<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use Carbon\CarbonImmutable;
use JMS\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use JMS\Serializer\SerializerBuilder;

class SecurityController extends AbstractController
{
    private Serializer $serializer;

    public function __construct()
    {
        $this->serializer = SerializerBuilder::create()->build();
    }


    #[Route('/login', name: 'api_login', methods: ['POST'])]
    public function login(#[CurrentUser] ?User $user): void
    {
        // Let it empty...
    }

    #[Route('/register', name: 'api_register', methods: ['POST'])]
    public function register(Request $request, UserPasswordHasherInterface $passwordHasher): JsonResponse
    {
        $user = $this->serializer->deserialize($request->getContent(), User::class, 'json');
        if(empty($user->getEmail()) || empty($user->getPassword())) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }
        if(in_array("ROLE_ADMIN", $user->getRoles(), true)){
            $user->setRoles(["ROLE_USER"]);
        }

        $user->setPassword($passwordHasher->hashPassword($user, $user->getPassword()));
        $user->setIsShown(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new JsonResponse(['status' => 'Customer created!'], Response::HTTP_CREATED);
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    #[Route('/logout', name: 'api_logout', methods: ['POST'])]
    public function logout(Security $security, UserRepository $userRepository ): Response
    {
        $userRepository->removeRefreshToken($security->getUser()->getUserIdentifier());
        return new Response(null, 200, ["Content-Type" => "application/json"]);
    }
}

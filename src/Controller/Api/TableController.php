<?php

namespace App\Controller\Api;

use App\Entity\Restaurant;
use App\Entity\Table;
use App\Repository\RestaurantRepository;
use App\Repository\TableRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant')]
class TableController extends AbstractController
{

    #[Route('/{id}/table', name: 'show_tables', methods: ['GET'])]
    public function tables(SerializerInterface $serializer, RestaurantRepository $restaurantRepository, Restaurant $restaurant): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $tables = $restaurant->getTables();

        $response = array("tables" => $tables);

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["table", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject,200, ['Content-Type' => 'application/json']);
    }

    #[Route('/{id}/table/{idTable}', name: 'show_table', methods: ['GET'])]
    public function table(
        SerializerInterface $serializer,
        String $idTable,
        RestaurantRepository $restaurantRepository,
        Restaurant $restaurant,
        TableRepository $tableRepository
    ): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $tables = $restaurant->getTables();
        $table = $tableRepository->find($idTable);

        if($tables->contains($table)) {
            $response = array("table" => $table);
            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["table", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject,200, ['Content-Type' => 'application/json']);
        }
        $error = array("message" => "Table introuvable");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/table', name: 'add_table', methods: ['POST'])]
    #[IsGranted("ROLE_MANAGER")]
    public function addTable(
        SerializerInterface $serializer,
        RestaurantRepository $restaurantRepository,
        String $id,
        Request $request,
        UserRepository $userRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $userRestaurants = $user->getRestaurants();
        $restaurant = $restaurantRepository->find($id);
        $payload = json_decode($request->getContent(), true);

        if($userRestaurants->contains($restaurant)) {
            $tablesAdded = [];

            for ($i = 0; $i < $payload['quantity']; $i++) {
                $table = new Table();
                $table->setNbPlace($payload['nb_place']);
                $table->setAvailable(true);
                $table->setTableNumero($payload['table_numero']);
                $table->setEveningService(Table::AVAILABLE);
                $table->setLunchService(Table::AVAILABLE);
                $em = $this->getDoctrine()->getManager();
                $restaurant->addTable($table);
                array_push($tablesAdded, $table);
                $em->persist($table);
                $em->flush();
            }

            $response = array("tables" => $tablesAdded, "message" => "Table(s) ajoutée(s)");

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["table", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
        }
        $error = array("message" => "Restaurant introuvable");
        return new Response(json_encode($error),400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/table/{tableId}', name: 'update_table', methods: ['PUT'])]
    #[IsGranted("ROLE_MANAGER")]
    public function updateTables(
        SerializerInterface $serializer,
        RestaurantRepository $restaurantRepository,
        String $id,
        String $tableId,
        TableRepository $tableRepository,
        Request $request,
        UserRepository $userRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $restaurant = $restaurantRepository->find($id);
        $tables = $restaurant->getTables();
        $table = $tableRepository->find($tableId);
        $payload = json_decode($request->getContent(), true);

        if($user->getRestaurants()->contains($restaurant)) {
            if($table and $tables->contains($table)) {
                $table->setNbPlace($payload['nb_place']);
                $table->setTableNumero($payload['table_numero']);
                $table->setAvailable($payload['available']);
                $table->setEveningService($payload['evening_service']);
                $table->setLunchService($payload['lunch_service']);
                $em = $this->getDoctrine()->getManager();
                $em->flush();
            } else {
                $error = array("message" => "Erreur lors de la mise à jour des tables");
                return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
            }
            $response = array("table" => $table, "message" => "Table mise à jour !");

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["table", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }
        $error = array("message" => "Table introuvalbe !");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/table/{tableId}', name: 'remove_table', methods: ['DELETE'])]
    #[IsGranted("ROLE_MANAGER")]
    public function removeTables(
        RestaurantRepository $restaurantRepository,
        String $tableId,
        Restaurant $restaurant,
        TableRepository $tableRepository,
        UserRepository $userRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $restaurant = $restaurantRepository->find($restaurant);
        $tables = $restaurant->getTables();
        $table = $tableRepository->find($tableId);

        if($user->getRestaurants()->contains($restaurant)) {
            if($tables->contains($table)) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($table);
                $em->flush();
            } else {
                $error = array("message" => "Erreur lors de la suppresion de la table");
                return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
            }
            $response = array("deleted_table_id" => $tableId, "message" => "Table supprimée");
            return new Response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
        $error = array("message" => "Erreur lors de la suppresion de table(s)");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }
}
<?php

namespace App\Controller\Api;

use App\Entity\Restaurant;
use App\Entity\User;
use App\Entity\UserWorkAtRestaurant;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use App\Repository\UserWorkAtRestaurantRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/restaurant')]
class UserWorkAtRestaurantController extends AbstractController
{
    #[Route('/{id}/user-restaurant', name: 'show_users_restaurants', methods: ['GET'])]
    #[Security("is_granted('ROLE_MANAGER') or is_granted('ROLE_OWNER')")]
    public function usersRestaurant(SerializerInterface $serializer, Restaurant $restaurant, RestaurantRepository $restaurantRepository): Response
    {
//        $userRestaurants = $userRepository->find($this->getUser()->getId())->getRestaurants();
            $restaurant = $restaurantRepository->find($restaurant);
            $usersRestaurant = $restaurant->getUsersRestaurant();

            $response = array("users" => $usersRestaurant);
            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["user", "timestamp", "restaurant"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/user-restaurant/{userWorkAtRestaurantId}', name: 'show_user_restaurants', methods: ['GET'])]
    #[IsGranted("ROLE_MANAGER")]
    public function userRestaurant(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        String $userWorkAtRestaurantId,
        RestaurantRepository $restaurantRepository,
        UserWorkAtRestaurantRepository $userWorkAtRestaurantRepository
    ): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $restaurantEmployee = $restaurant->getUsersRestaurant();
        $userWorkAtRestaurant = $userWorkAtRestaurantRepository->find($userWorkAtRestaurantId);

        if($restaurantEmployee->contains($userWorkAtRestaurant)) {

            $response = array("user" => $userWorkAtRestaurant);
            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["user", "timestamp", "restaurant"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }
        $error = array("message" => "Cet employé n'existe pas");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/user-restaurant/{userWorkAtRestaurantId}', name: 'remove_user_restaurants', methods: ['DELETE'])]
    #[IsGranted("ROLE_OWNER")]
    public function removeUserRestaurant(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        UserRepository $userRepository,
        String $userWorkAtRestaurantId,
        RestaurantRepository $restaurantRepository,
        UserWorkAtRestaurantRepository $userWorkAtRestaurantRepository
    ): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $restaurantEmployee = $restaurant->getUsersRestaurant();
//        return new Response(json_encode($restaurantEmployee), 200, ["Content-Type" => "application/json"]);
        $userWorkAtRestaurant = $userRepository->find($userWorkAtRestaurantId);
        if($restaurantEmployee->contains($userWorkAtRestaurant)) {
            $deletedUserId = $userWorkAtRestaurant->getId();
            $response = array("deleted_user_id" => $deletedUserId, "message" => "Utilisateur supprimé");
            $em = $this->getDoctrine()->getManager();
            $em->remove($userWorkAtRestaurant);
            $em->flush();

            return new Response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
        $error = array("message" => "Cet employé n'existe pas");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}/user-restaurant/{userWorkAtRestaurantId}', name: 'update_user_restaurants', methods: ['PUT'])]
    #[IsGranted("ROLE_OWNER")]
    public function updateUserRestaurant(
        SerializerInterface $serializer,
        Restaurant $restaurant,
        String $userWorkAtRestaurantId,
        RestaurantRepository $restaurantRepository,
        UserWorkAtRestaurantRepository $userWorkAtRestaurantRepository,
        Request $request,
        UserRepository $userRepository
    ): Response
    {
        $restaurant = $restaurantRepository->find($restaurant);
        $restaurantEmployee = $restaurant->getUsersRestaurant();
        $userWorkAtRestaurant = $userRepository->find($userWorkAtRestaurantId);
        $payload = json_decode($request->getContent(), true);

        if($restaurantEmployee->contains($userWorkAtRestaurant)) {

            $user = $userRepository->find($userWorkAtRestaurant->getId());
            $user->setRoles($payload['roles']);
            $user->setFirstname($payload['firstname']);
            $user->setLastname($payload['lastname']);
            $user->setEmail($payload['email']);

            $response = array("user" => $user, "message" => "Utilisateur mis à jour");

            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["user", "timestamp", "restaurant"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
        }

        $error = array("message" => "Cet employé n'existe pas");
        return new Response(json_encode($error), 400, ["Content-Type" => "application/json"]);
    }
}
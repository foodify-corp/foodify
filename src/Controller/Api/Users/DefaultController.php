<?php

namespace App\Controller\Api\Users;

use App\Entity\Restaurant;
use App\Entity\User;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use DateTime;
use PHPUnit\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\Exception\ExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use function PHPUnit\Framework\throwException;

#[Route('/users')]
class DefaultController extends AbstractController
{
    #[Route('', name: 'users', methods: ['GET'])]
    public function getUsers(SerializerInterface $serializer, UserRepository $userRepository): Response
    {
        //TODO implements JMS Serializer
        //TODO function who handle circular references
        $response = array('users' => $userRepository->findAll());

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["user", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject,200, ["Content-Type" => "application/json"]);
    }

    #[Route('/{id}', name: 'update_user', methods: ['PUT'])]
    public function updateCategory(
        SerializerInterface $serializer,
        Request $request,
        String $categoryId,
        RestaurantRepository $restaurantRepository,
        UserRepository $userRepository
    ): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $payload = json_decode($request->getContent(), true);

        $em = $this->getDoctrine()->getManager();
        $user->setFirstname($payload['firstname']);
        $user->setLastname($payload['firstname']);
        $em->flush();

        $response = array("user" => $user);

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["user", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
    }

    #[Route('/restaurant', name: 'show_user_owned_restaurants', methods: ['GET'])]
    #[isGranted('ROLE_OWNER')]
    public function userRestaurants(SerializerInterface $serializer, UserRepository $userRepository): Response
    {
        $userRestaurants = $userRepository->find($this->getUser()->getId())->getRestaurants();
        $response = array("restaurants" => $userRestaurants);

        if($userRestaurants) {
            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["restaurant", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
        }

        return new Response(json_encode([]));
    }

    #[Route('/restaurant/{id}', name: 'show_user_owned_restaurant', methods: ['GET'])]
    #[isGranted('ROLE_OWNER')]
    public function userRestaurant(
        SerializerInterface $serializer,
        RestaurantRepository $restaurantRepository,
        Restaurant $restaurant,
        UserRepository $userRepository
    ): Response
    {
        $userRestaurants = $userRepository->find($this->getUser()->getId())->getRestaurants();
        $restaurant = $restaurantRepository->find($restaurant);
        $response = array("restaurant" => $restaurant);

        if($userRestaurants->contains($restaurant)) {
            $jsonObject = $serializer->serialize($response, 'json', [
                'groups' => ["restaurant", "timestamp"],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);
            return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
        }

        return new Response(json_encode([]));
    }

    #[Route('/me', name: 'users_me', methods: ['GET'])]
    public function getMe(Security $security, UserRepository $userRepository, SerializerInterface $serializer): Response
    {
        $user = $userRepository->find($security->getUser()->getId());
        $response = array('user' => $user);

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["user", "timestamp", "user_restaurant", "restaurant"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
    }

    #[Route('/update-user', name: 'update_user', methods: ['PUT'])]
    public function updateUser(Request $request, Security $security, UserRepository $userRepository, SerializerInterface $serializer): Response
    {
        $user = $userRepository->find($security->getUser()->getId());
        $payload = json_decode($request->getContent(), true);

        $em = $this->getDoctrine()->getManager();

        try {
            $user->setTwitterUrl($payload['twitter_url']);
            $user->setFacebookUrl($payload['facebook_url']);
            $user->setInstagramUrl($payload['instagram_url']);
            $user->setFirstname($payload['firstname']);
            $user->setLastname($payload['lastname']);
            if($payload['birthday'] != "") {
                $user->setBirthday(\DateTime::createFromFormat('Y-m-d', $payload['birthday']));
            } else {
                $user->setBirthday(null);
            }
            $user->setPhone($payload['phone']);

            $em->flush();
        } catch (Exception $e) {
            return new Response(json_encode(['message' => $e->getMessage()]));
        }

        $response = array('user' => $user);

        $jsonObject = $serializer->serialize($response, 'json', [
            'groups' => ["user", "timestamp"],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ["Content-Type" => "application/json"]);
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Restaurant;
use App\Entity\Dish;
use App\Entity\Categories;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RestaurantFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
        $this->faker = \Faker\Factory::create();

        for ($i = 0; $i < 5; $i++) {

            $kitchenStyles = ['chinois', 'thaï', 'tacos', 'méxicain', 'burger', 'pizza', 'pâtes'];
            $restaurant = (new Restaurant())
                ->setName($this->faker->word)
                ->setAddress($this->faker->streetAddress)
                ->setCity($this->faker->city)
                ->setCp($this->faker->postcode)
                ->setCountry($this->faker->country)
                ->setActivity($this->faker->word)
                ->setNSiren($this->faker->randomNumber($nbDigits = 9, $strict = true))
                ->setKitchenStyle($kitchenStyles[array_rand($kitchenStyles, 1)])
                ->setTakeAway($this->faker->boolean)
                ->setOnPlace($this->faker->boolean)
                ->setInstagramUrl('https://www.instagram.com')
                ->setFacebookUrl('https://www.facebook.com')
                ->setTwitterUrl('https://www.twitter.com')    
                ;

            $manager->persist($restaurant);

            for ($j = 0; $j < 15; $j++) {

                $dish = (new Dish())
                    ->setName($this->faker->word)
                    ->setPrice($this->faker->randomNumber(2))
                    ->setIsAvailable($this->faker->boolean)
                    ->setIsAvailableTime(['noon', 'evening'])
                    ->setRestaurantId($restaurant);
    
                $manager->persist($dish);
            
            }

            for ($k = 0; $k < 10; $k++) {

                $category = (new Categories())
                    ->setName($this->faker->word)
                    ->setRestaurantId($restaurant);;
    
                $manager->persist($category);
            
            }
        }

        $manager->flush();
    }
}
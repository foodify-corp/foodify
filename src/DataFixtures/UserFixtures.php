<?php

namespace App\DataFixtures;

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $this->faker = \Faker\Factory::create();

        $admin = (new User())
            ->setEmail('admin@admin')
            ->setFirstname('admin')
            ->setLastname('admin')
            ->setRoles(['ROLE_SUPER_ADMIN'])
            ->setIsShown($this->faker->boolean);

        $admin->setPassword($this->userPasswordHasher->hashPassword($admin, 'admin'));
        $manager->persist($admin);

        for ($i = 0; $i < 30; $i++) {

            $object = (new User())
                ->setEmail($this->faker->email)
                ->setFirstname($this->faker->streetName)
                ->setLastname($this->faker->streetName)
                ->setRoles(['ROLE_USER'])
                ->setUpdatedAt(Carbon::now())
                ->setCreatedAt(Carbon::now())
                ->setIsShown($this->faker->boolean);

            $object->setPassword($this->userPasswordHasher->hashPassword($object, 'password'));
            $manager->persist($object);
        }

        $manager->flush();
    }
}
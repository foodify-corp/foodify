<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\CategoriesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Type;

/**
 * @ORM\Entity(repositoryClass=CategoriesRepository::class)
 */
class Categories
{
    use TimestampableCarbonTrait;

    /**
     * @Groups("category")
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     */
    private $id;

    /**
     * @Groups("category")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="categories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant_id;

    /**
     * @ORM\OneToOne(targetEntity=CategoriesDishes::class, mappedBy="category", cascade={"persist", "remove"})
     */
    private $categoriesDishes;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRestaurantId(): ?Restaurant
    {
        return $this->restaurant_id;
    }

    public function setRestaurantId(?Restaurant $restaurant_id): self
    {
        $this->restaurant_id = $restaurant_id;

        return $this;
    }

    public function getCategoriesDishes(): ?CategoriesDishes
    {
        return $this->categoriesDishes;
    }

    public function setCategoriesDishes(?CategoriesDishes $categoriesDishes): self
    {
        // unset the owning side of the relation if necessary
        if ($categoriesDishes === null && $this->categoriesDishes !== null) {
            $this->categoriesDishes->setCategory(null);
        }

        // set the owning side of the relation if necessary
        if ($categoriesDishes !== null && $categoriesDishes->getCategory() !== $this) {
            $categoriesDishes->setCategory($this);
        }

        $this->categoriesDishes = $categoriesDishes;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\CategoriesDishesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CategoriesDishesRepository::class)
 */
class CategoriesDishes
{
    use TimestampableCarbonTrait;

    /**
     * @Groups("category_dishes")
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     */
    private $id;

    /**
     * @Groups("category_dishes")
     * @ORM\ManyToMany(targetEntity=Dish::class, inversedBy="categoriesDishes")
     */
    private $dishes;

    /**
     * @Groups("category_dishes")
     * @ORM\OneToOne(targetEntity=Categories::class, inversedBy="categoriesDishes", cascade={"persist", "remove"})
     */
    private $category;

    public function __construct()
    {
        $this->dishes = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return Collection|Dish[]
     */
    public function getDishes(): Collection
    {
        return $this->dishes;
    }

    public function addDishes(Dish $dishes): self
    {
        if (!$this->dishes->contains($dishes)) {
            $this->dishes[] = $dishes;
        }

        return $this;
    }

    public function removeDish(Dish $dishes): self
    {
        $this->dishes->removeElement($dishes);

        return $this;
    }

    public function getCategory(): ?Categories
    {
        return $this->category;
    }

    public function setCategory(?Categories $category): self
    {
        $this->category = $category;

        return $this;
    }
}

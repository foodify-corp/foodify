<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\DishRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=DishRepository::class)
 * @ORM\Table(name="`dish`")
 */
class Dish
{
    use TimestampableCarbonTrait;
    /**
     * @Groups("dish")
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     */
    private $id;

    /**
     * @Groups({"get_order_dish", "dish"})
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $name;

    /**
     * @Groups({"get_order_dish", "dish"})
     * @ORM\Column(type="float", scale=2)
     * @Type("float")
     */
    private $price;

    /**
     * @Groups("dish")
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $is_available;

    /**
     * @Groups("dish")
     * @ORM\Column(type="array", nullable=true)
     * @Type("array")
     */
    private $is_available_time = [];

    /**
     * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="dishes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant_id;

    /**
     * @ORM\ManyToMany(targetEntity=CategoriesDishes::class, mappedBy="dishes")
     */
    private $categoriesDishes;

    /**
     * @ORM\ManyToMany(targetEntity=MenuDishes::class, mappedBy="dishes")
     */
    private $menuDishes;

    /**
     * @ORM\OneToMany(targetEntity=OrderDish::class, mappedBy="dish", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $orderDishes;


    /**
     * @Groups("dish")
     * @Vich\UploadableField(mapping="dishes_images", fileNameProperty="imageName", size="imageSize")
     *
     * @var File|null
     */
    private $dishFile;

    /**
     * @Groups("dish")
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private ?string $imageName = null;

    /**
     * @Groups("dish")
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private ?int $imageSize;

    public function __construct()
    {
        $this->categoriesDishes = new ArrayCollection();
        $this->menuDishes = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIsAvailable(): ?bool
    {
        return $this->is_available;
    }

    public function setIsAvailable(bool $is_available): self
    {
        $this->is_available = $is_available;

        return $this;
    }

    public function getIsAvailableTime(): ?array
    {
        return $this->is_available_time;
    }

    public function setIsAvailableTime(?array $is_available_time): self
    {
        $this->is_available_time = $is_available_time;

        return $this;
    }

    public function getRestaurantId(): ?Restaurant
    {
        return $this->restaurant_id;
    }

    public function setRestaurantId(?Restaurant $restaurant_id): self
    {
        $this->restaurant_id = $restaurant_id;

        return $this;
    }

    /**
     * @return Collection|CategoriesDishes[]
     */
    public function getCategoriesDishes(): Collection
    {
        return $this->categoriesDishes;
    }

    public function addCategoriesDish(CategoriesDishes $categoriesDish): self
    {
        if (!$this->categoriesDishes->contains($categoriesDish)) {
            $this->categoriesDishes[] = $categoriesDish;
            $categoriesDish->addDishes($this);
        }

        return $this;
    }

    public function removeCategoriesDish(CategoriesDishes $categoriesDish): self
    {
        if ($this->categoriesDishes->removeElement($categoriesDish)) {
            $categoriesDish->removeDish($this);
        }

        return $this;
    }

    /**
     * @return Collection|MenuDishes[]
     */
    public function getMenuDishes(): Collection
    {
        return $this->menuDishes;
    }

    public function addMenuDish(MenuDishes $menuDish): self
    {
        if (!$this->menuDishes->contains($menuDish)) {
            $this->menuDishes[] = $menuDish;
            $menuDish->addDishes($this);
        }

        return $this;
    }

    public function removeMenuDish(MenuDishes $menuDish): self
    {
        if ($this->menuDishes->removeElement($menuDish)) {
            $menuDish->removeDish($this);
        }

        return $this;
    }

    /**
     * @return Collection|OrderDish[]
     */
    public function getOrderDishes(): Collection
    {
        return $this->orderDishes;
    }

    public function addOrderDish(OrderDish $orderDish): self
    {
        if (!$this->orderDishes->contains($orderDish)) {
            $this->orderDishes[] = $orderDish;
            $orderDish->setDish($this);
        }

        return $this;
    }

    public function removeOrderDish(OrderDish $orderDish): self
    {
        if ($this->orderDishes->removeElement($orderDish)) {
            // set the owning side to null (unless already changed)
            if ($orderDish->getDish() === $this) {
                $orderDish->setDish(null);
            }
        }

        return $this;
    }


    /**
     * @param File|null $restaurantFile
     * @return Dish
     */
    public function setRestaurantFile(?File $dishFile = null): self
    {
        $this->dishFile = $dishFile;

        if ($this->dishFile instanceof UploadedFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    public function getDishFile(): ?File
    {
        return $this->dishFile;
    }

    public function setImageName(?string $imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }

    public function getImageName(): ?string
    {
        return $this->imageName ?: "";
    }

    public function setImageSize(?int $imageSize): self
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }
}
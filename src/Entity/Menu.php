<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\MenuRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Type;

/**
 * @ORM\Entity(repositoryClass=MenuRepository::class)
 */
class Menu
{
    use TimestampableCarbonTrait;
    /**
     * @Groups("menu")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @Groups("menu")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $name;

    /**
     * @Groups("menu")
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $display;

    /**
     * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="menus")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant_id;

    /**
     * @ORM\OneToOne(targetEntity=MenuDishes::class, mappedBy="menu", cascade={"persist", "remove"})
     */
    private $menuDishes;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDisplay(): ?bool
    {
        return $this->display;
    }

    public function setDisplay(bool $display): self
    {
        $this->display = $display;

        return $this;
    }

    public function getMenuDishes(): ?MenuDishes
    {
        return $this->menuDishes;
    }

    public function setMenuDishes(?MenuDishes $menuDishes): self
    {
        $this->menuDishes = $menuDishes;

        return $this;
    }

    public function getRestaurantId(): ?Restaurant
    {
        return $this->restaurant_id;
    }

    public function setRestaurantId(?Restaurant $restaurant_id): self
    {
        $this->restaurant_id = $restaurant_id;

        return $this;
    }
}

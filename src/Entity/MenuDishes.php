<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\MenuDishesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Type;

/**
 * @ORM\Entity(repositoryClass=MenuDishesRepository::class)
 */
class MenuDishes
{
    use TimestampableCarbonTrait;
    /**
     * @Groups("menu_dish")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @Groups("menu_dish")
     * @ORM\Column(type="float", scale=2, nullable=true)
     * @Type("float")
     */
    private $price;

    /**
     * @Groups("menu_dish")
     * @ORM\ManyToMany(targetEntity=Dish::class, inversedBy="menuDishes")
     */
    private $dishes;

    /**
     * @Groups("menu_dish")
     * @ORM\OneToOne(targetEntity=Menu::class, inversedBy="menuDishes", cascade={"persist", "remove"})
     */
    private $menu;

    public function __construct()
    {
        $this->dishes = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|Dish[]
     */
    public function getDishes(): Collection
    {
        return $this->dishes;
    }

    public function addDishes(Dish $dishes): self
    {
        if (!$this->dishes->contains($dishes)) {
            $this->dishes[] = $dishes;
        }

        return $this;
    }

    public function removeDish(Dish $dishes): self
    {
        $this->dishes->removeElement($dishes);

        return $this;
    }

    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }
}

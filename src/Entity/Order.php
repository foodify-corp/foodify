<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    const STATUS_PENDING = "Pending";
    const STATUS_ACCEPTED = "Accepted";
    const STATUS_DONE = "Done";

    const ORDER_TYPE_ON_SITE = "On site";
    const ORDER_TYPE_TAKE_AWAY = "Take away";

    use TimestampableCarbonTrait;
    /**
     * @Groups({"get_orders"})
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     */
    private $id;

    /**
     * @Groups({"get_orders"})
     * @ORM\Column(type="string", length=255)
     */
    private $order_type;

    /**
     * @ORM\ManyToOne(targetEntity=restaurant::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
     * @ORM\JoinColumn(name="`user`", nullable=false)
     */
    private $user;

    /**
     * @Groups({"get_orders"})
     * @ORM\OneToMany(targetEntity=OrderDish::class, mappedBy="order", cascade={"persist"})
     */
    private $order_dishes;

    /**
     * @Groups({"get_orders"})
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    public function __construct()
    {
        $this->order_dishes = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getOrderType(): ?string
    {
        return $this->order_type;
    }

    public function setOrderType(string $order_type): self
    {
        if ($order_type !== self::ORDER_TYPE_ON_SITE && $order_type !== self::ORDER_TYPE_TAKE_AWAY) {
            throw new \InvalidArgumentException("Order status must be equal to " . self::ORDER_TYPE_ON_SITE . " or " . self::ORDER_TYPE_TAKE_AWAY);
        }
        $this->order_type = $order_type;

        return $this;
    }

    public function getRestaurant(): ?restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|OrderDish[]
     */
    public function getOrderDishes(): Collection
    {
        return $this->order_dishes;
    }

    public function addOrderDish(OrderDish $order_dish): self
    {
        if (!$this->order_dishes->contains($order_dish)) {
            $this->order_dishes[] = $order_dish;
            $order_dish->setOrder($this);
        }

        return $this;
    }

    public function removeOrderDish(OrderDish $order_dish): self
    {
        if ($this->order_dishes->removeElement($order_dish)) {
            // set the owning side to null (unless already changed)
            if ($order_dish->getOrder() === $this) {
                $order_dish->setOrder(null);
            }
        }

        return $this;
    }

    public function removeAllOrderDish(): self
    {
        // the following seemingly works
        foreach ($this->order_dishes as $order_dish) {
            $this->order_dishes->removeElement($order_dish);
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if ($status !== self::STATUS_PENDING && $status !== self::STATUS_ACCEPTED && $status !== self::STATUS_DONE) {
            throw new \InvalidArgumentException("Order status must be equal to " . self::STATUS_PENDING . ", " . self::STATUS_ACCEPTED . " or " . self::STATUS_DONE);
        }

        $this->status = $status;

        return $this;
    }
}

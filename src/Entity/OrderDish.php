<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\OrderDishRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OrderDishRepository::class)
 */
class OrderDish
{
    use TimestampableCarbonTrait;

    /**
     * @Groups({"get_dish"})
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     */
    private $id;

    /**
     * @Groups({"get_dish"})
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @Groups({"get_dish"})
     * @ORM\ManyToOne(targetEntity=Dish::class, inversedBy="orders_dishes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $dish;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="order_dishes")
     * @ORM\JoinColumn(name="`order`", nullable=false)
     */
    private $order;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getDish(): ?Dish
    {
        return $this->dish;
    }

    public function setDish(?Dish $dish): self
    {
        $this->dish = $dish;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }
}

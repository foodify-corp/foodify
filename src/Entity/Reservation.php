<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\ReservationRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    use TimestampableCarbonTrait;

    const ACCEPTED = "Accepted";
    const PENDING = "Pending";
    const CANCELED = "Canceled";
    const REFUSED = "Refused";
    const AVAILABLE = "Available";

    /**
     * @Groups("reservation")
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     */
    private $id;

    /**
     * @Groups("reservation")
     * @ORM\Column(type="datetime_immutable")
     * @Type("DateTimeImmutable")
     */
    private $reservation_date;

    /**
     * @Groups("reservation")
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $nb_people;

    /**
     * @Groups("reservation")
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $is_validated;

    /**
     * @Groups("reservation")
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Type("DateTimeImmutable")
     */
    private $confirmed_at;

    /**
     * @Groups("reservation")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant_id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_id;

    /**
     * @Groups("reservation")
     * @ORM\ManyToOne(targetEntity=Table::class, inversedBy="reservations")
     */
    private $table_id;

    /**
     * @Groups("reservation")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $name;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getReservationDate(): ?\DateTimeImmutable
    {
        return $this->reservation_date;
    }

    public function setReservationDate(\DateTimeImmutable $reservation_date): self
    {
        $this->reservation_date = $reservation_date;

        return $this;
    }

    public function getNbPeople(): ?int
    {
        return $this->nb_people;
    }

    public function setNbPeople(int $nb_people): self
    {
        $this->nb_people = $nb_people;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->is_validated;
    }

    public function setIsValidated(bool $is_validated): self
    {
        $this->is_validated = $is_validated;

        return $this;
    }

    public function getConfirmedAt(): ?\DateTimeImmutable
    {
        return $this->confirmed_at;
    }

    public function setConfirmedAt(\DateTimeImmutable $confirmed_at): self
    {
        $this->confirmed_at = $confirmed_at;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if($status != self::AVAILABLE and $status != self::CANCELED and $status != self::REFUSED and $status != self::ACCEPTED and $status != self::PENDING) {
            throw new InvalidArgumentException("Status must be equal to 'accepted', 'declined', 'pending', 'canceled' or 'refused', status: $status");
        } else {
            $this->status = $status;
        }

        return $this;
    }

    public function getRestaurantId(): ?Restaurant
    {
        return $this->restaurant_id;
    }

    public function setRestaurantId(?Restaurant $restaurant_id): self
    {
        $this->restaurant_id = $restaurant_id;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getTableId(): ?Table
    {
        return $this->table_id;
    }

    public function setTableId(?Table $table_id): self
    {
        $this->table_id = $table_id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}

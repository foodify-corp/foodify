<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\RestaurantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=RestaurantRepository::class)
 * @Vich\Uploadable()
 */
class Restaurant
{
    use TimestampableCarbonTrait;

    /**
     * @Groups("restaurant")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $name;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $address;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $city;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $cp;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $country;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $activity;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $n_siren;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255)
     * @Type("string")
     */
    private $kitchen_style;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $take_away;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $on_place;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Type("string")
     */
    private $instagram_url;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Type("string")
     */
    private $facebook_url;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Type("string")
     */
    private $twitter_url;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="restaurants")
     * @ORM\JoinColumn(nullable=true)
     */
    private $owner_user_id;

    /**
     * @ORM\OneToMany(targetEntity=Dish::class, mappedBy="restaurant_id", orphanRemoval=true)
     */
    private $dishes;

    /**
     * @ORM\OneToMany(targetEntity=Table::class, mappedBy="restaurant_id", orphanRemoval=true)
     */
    private $tables;

    /**
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="restaurant_id", orphanRemoval=true)
     */
    private $reservations;

    /**
     * @ORM\OneToMany(targetEntity=UserWorkAtRestaurant::class, mappedBy="restaurant_id", orphanRemoval=true)
     */
    private $userWorkAtRestaurants;

    /**
     * @ORM\OneToMany(targetEntity=Menu::class, mappedBy="restaurant_id", orphanRemoval=true)
     */
    private $menus;

    /**
     * @ORM\OneToMany(targetEntity=Categories::class, mappedBy="restaurant_id", orphanRemoval=true)
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="restaurant", orphanRemoval=true)
     */
    private $usersRestaurant;

    /**
     * @Groups("restaurant")
     * @ORM\OneToOne(targetEntity=RestaurantTimes::class, mappedBy="restaurant", cascade={"persist", "remove"})
     */
    private $restaurantTimes;

    /**
     * @Groups("restaurant")
     * @Vich\UploadableField(mapping="restaurants_images", fileNameProperty="imageName", size="imageSize")
     *
     * @var File|null
     */
    private $restaurantFile;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private ?string $imageName = null;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private ?int $imageSize;

    /**
     * @ORM\OneToMany(targetEntity=RestaurantComments::class, mappedBy="restaurant")
     */
    private $restaurantComments;

    public function __construct()
    {
        $this->dishes = new ArrayCollection();
        $this->tables = new ArrayCollection();
        $this->reservations = new ArrayCollection();
        $this->userWorkAtRestaurants = new ArrayCollection();
        $this->menus = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->usersRestaurant = new ArrayCollection();
        $this->restaurantComments = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getActivity(): ?string
    {
        return $this->activity;
    }

    public function setActivity(string $activity): self
    {
        $this->activity = $activity;

        return $this;
    }

    public function getNSiren(): ?string
    {
        return $this->n_siren;
    }

    public function setNSiren(string $n_siren): self
    {
        $this->n_siren = $n_siren;

        return $this;
    }

    public function getKitchenStyle(): ?string
    {
        return $this->kitchen_style;
    }

    public function setKitchenStyle(string $kitchen_style): self
    {
        $this->kitchen_style = $kitchen_style;

        return $this;
    }

    public function getTakeAway(): ?bool
    {
        return $this->take_away;
    }

    public function setTakeAway(bool $take_away): self
    {
        $this->take_away = $take_away;

        return $this;
    }

    public function getOnPlace(): ?bool
    {
        return $this->on_place;
    }

    public function setOnPlace(bool $on_place): self
    {
        $this->on_place = $on_place;

        return $this;
    }

    public function getInstagramUrl(): ?string
    {
        return $this->instagram_url;
    }

    public function setInstagramUrl(?string $instagram_url): self
    {
        $this->instagram_url = $instagram_url;

        return $this;
    }

    public function getFacebookUrl(): ?string
    {
        return $this->facebook_url;
    }

    public function setFacebookUrl(?string $facebook_url): self
    {
        $this->facebook_url = $facebook_url;

        return $this;
    }

    public function getTwitterUrl(): ?string
    {
        return $this->twitter_url;
    }

    public function setTwitterUrl(?string $twitter_url): self
    {
        $this->twitter_url = $twitter_url;

        return $this;
    }

    public function getOwnerUserId(): ?User
    {
        return $this->owner_user_id;
    }

    public function setOwnerUserId(?User $owner_user_id): self
    {
        $this->owner_user_id = $owner_user_id;

        return $this;
    }

    /**
     * @return Collection|Dish[]
     */
    public function getDishes(): Collection
    {
        return $this->dishes;
    }

    public function addDish(Dish $dish): self
    {
        if (!$this->dishes->contains($dish)) {
            $this->dishes[] = $dish;
            $dish->setRestaurantId($this);
        }

        return $this;
    }

    public function removeDish(Dish $dish): self
    {
        if ($this->dishes->removeElement($dish)) {
            // set the owning side to null (unless already changed)
            if ($dish->getRestaurantId() === $this) {
                $dish->setRestaurantId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Table[]
     */
    public function getTables(): Collection
    {
        return $this->tables;
    }

    public function addTable(Table $table): self
    {
        if (!$this->tables->contains($table)) {
            $this->tables[] = $table;
            $table->setRestaurantId($this);
        }

        return $this;
    }

    public function removeTable(Table $table): self
    {
        if ($this->tables->removeElement($table)) {
            // set the owning side to null (unless already changed)
            if ($table->getRestaurantId() === $this) {
                $table->setRestaurantId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setRestaurantId($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getRestaurantId() === $this) {
                $reservation->setRestaurantId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserWorkAtRestaurant[]
     */
    public function getUserWorkAtRestaurants(): Collection
    {
        return $this->userWorkAtRestaurants;
    }

    public function addUserWorkAtRestaurant(UserWorkAtRestaurant $userWorkAtRestaurant): self
    {
        if (!$this->userWorkAtRestaurants->contains($userWorkAtRestaurant)) {
            $this->userWorkAtRestaurants[] = $userWorkAtRestaurant;
            $userWorkAtRestaurant->setRestaurant($this);
        }

        return $this;
    }

    public function removeUserWorkAtRestaurant(UserWorkAtRestaurant $userWorkAtRestaurant): self
    {
        if ($this->userWorkAtRestaurants->removeElement($userWorkAtRestaurant)) {
            // set the owning side to null (unless already changed)
            if ($userWorkAtRestaurant->getRestaurant() === $this) {
                $userWorkAtRestaurant->setRestaurant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenus(): Collection
    {
        return $this->menus;
    }

    public function addMenu(Menu $menu): self
    {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
            $menu->setRestaurantId($this);
        }

        return $this;
    }

    public function removeMenu(Menu $menu): self
    {
        if ($this->menus->removeElement($menu)) {
            // set the owning side to null (unless already changed)
            if ($menu->getRestaurantId() === $this) {
                $menu->setRestaurantId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Categories[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Categories $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->setRestaurantId($this);
        }

        return $this;
    }

    public function removeCategory(Categories $category): self
    {
        if ($this->categories->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getRestaurantId() === $this) {
                $category->setRestaurantId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsersRestaurant(): Collection
    {
        return $this->usersRestaurant;
    }

    public function addUsersRestaurant(User $usersRestaurant): self
    {
        if (!$this->usersRestaurant->contains($usersRestaurant)) {
            $this->usersRestaurant[] = $usersRestaurant;
            $usersRestaurant->setRestaurant($this);
        }

        return $this;
    }
    /**
     * @return Collection<int, RestaurantComments>
     */
    public function getRestaurantComments(): Collection
    {
        return $this->restaurantComments;
    }

    public function addRestaurantComment(RestaurantComments $restaurantComment): self
    {
        if (!$this->restaurantComments->contains($restaurantComment)) {
            $this->restaurantComments[] = $restaurantComment;
            $restaurantComment->setRestaurant($this);
        }

        return $this;
    }

    /**
     * @param File|null $restaurantFile
     * @return Restaurant
     */
    public function setRestaurantFile(?File $restaurantFile = null): self
    {
        $this->restaurantFile = $restaurantFile;

        if ($this->restaurantFile instanceof UploadedFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    public function removeUsersRestaurant(User $usersRestaurant): self
    {
        if ($this->usersRestaurant->removeElement($usersRestaurant)) {
            // set the owning side to null (unless already changed)
            if ($usersRestaurant->getRestaurant() === $this) {
                $usersRestaurant->setRestaurant(null);
            }
        }
        return $this;
    }

    public function getRestaurantFile(): ?File
    {
        return $this->restaurantFile;
    }

    public function setImageName(?string $imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }

    public function getRestaurantTimes(): ?RestaurantTimes
    {
        return $this->restaurantTimes;
    }

    public function setRestaurantTimes(?RestaurantTimes $restaurantTimes): self
    {
        // unset the owning side of the relation if necessary
        if ($restaurantTimes === null && $this->restaurantTimes !== null) {
            $this->restaurantTimes->setRestaurant(null);
        }

        // set the owning side of the relation if necessary
        if ($restaurantTimes !== null && $restaurantTimes->getRestaurant() !== $this) {
            $restaurantTimes->setRestaurant($this);
        }

        $this->restaurantTimes = $restaurantTimes;

        return $this;
    }

    public function getImageName(): ?string
    {
        return $this->imageName ?: "";
    }

    public function setImageSize(?int $imageSize): self
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }

    public function removeRestaurantComment(RestaurantComments $restaurantComment): self
    {
        if ($this->restaurantComments->removeElement($restaurantComment)) {
            // set the owning side to null (unless already changed)
            if ($restaurantComment->getRestaurant() === $this) {
                $restaurantComment->setRestaurant(null);
            }
        }

        return $this;
    }
}

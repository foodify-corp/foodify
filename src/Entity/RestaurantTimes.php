<?php

namespace App\Entity;

use App\Repository\RestaurantTimesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=RestaurantTimesRepository::class)
 */
class RestaurantTimes
{
    /**
     * @Groups("restaurant")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $mondayOpenAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $mondayCloseAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $tuesdayOpenAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $tuesdayCloseAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $wednesdayOpenAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $wednesdayCloseAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $thursdayOpenAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $thursdayCloseAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $fridayOpenAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $fridayCloseAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $saturdayOpenAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $saturdayCloseAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $sundayOpenAt;

    /**
     * @Groups("restaurant")
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $sundayCloseAt;

    /**
     * @ORM\OneToOne(targetEntity=Restaurant::class, inversedBy="restaurantTimes", cascade={"persist", "remove"})
     */
    private $restaurant;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getMondayOpenAt(): ?int
    {
        return $this->mondayOpenAt;
    }

    public function setMondayOpenAt(?int $mondayOpenAt): self
    {
        $this->mondayOpenAt = $mondayOpenAt;

        return $this;
    }

    public function getMondayCloseAt(): ?int
    {
        return $this->mondayCloseAt;
    }

    public function setMondayCloseAt(?int $mondayCloseAt): self
    {
        $this->mondayCloseAt = $mondayCloseAt;

        return $this;
    }

    public function getTuesdayOpenAt(): ?int
    {
        return $this->tuesdayOpenAt;
    }

    public function setTuesdayOpenAt(?int $tuesdayOpenAt): self
    {
        $this->tuesdayOpenAt = $tuesdayOpenAt;

        return $this;
    }

    public function getTuesdayCloseAt(): ?int
    {
        return $this->tuesdayCloseAt;
    }

    public function setTuesdayCloseAt(?int $tuesdayCloseAt): self
    {
        $this->tuesdayCloseAt = $tuesdayCloseAt;

        return $this;
    }

    public function getWednesdayOpenAt(): ?int
    {
        return $this->wednesdayOpenAt;
    }

    public function setWednesdayOpenAt(?int $wednesdayOpenAt): self
    {
        $this->wednesdayOpenAt = $wednesdayOpenAt;

        return $this;
    }

    public function getWednesdayCloseAt(): ?int
    {
        return $this->wednesdayCloseAt;
    }

    public function setWednesdayCloseAt(?int $wednesdayCloseAt): self
    {
        $this->wednesdayCloseAt = $wednesdayCloseAt;

        return $this;
    }

    public function getThursdayOpenAt(): ?int
    {
        return $this->thursdayOpenAt;
    }

    public function setThursdayOpenAt(?int $thursdayOpenAt): self
    {
        $this->thursdayOpenAt = $thursdayOpenAt;

        return $this;
    }

    public function getThursdayCloseAt(): ?int
    {
        return $this->thursdayCloseAt;
    }

    public function setThursdayCloseAt(?int $thursdayCloseAt): self
    {
        $this->thursdayCloseAt = $thursdayCloseAt;

        return $this;
    }

    public function getFridayOpenAt(): ?int
    {
        return $this->fridayOpenAt;
    }

    public function setFridayOpenAt(?int $fridayOpenAt): self
    {
        $this->fridayOpenAt = $fridayOpenAt;

        return $this;
    }

    public function getFridayCloseAt(): ?int
    {
        return $this->fridayCloseAt;
    }

    public function setFridayCloseAt(?int $fridayCloseAt): self
    {
        $this->fridayCloseAt = $fridayCloseAt;

        return $this;
    }

    public function getSaturdayOpenAt(): ?int
    {
        return $this->saturdayOpenAt;
    }

    public function setSaturdayOpenAt(?int $saturdayOpenAt): self
    {
        $this->saturdayOpenAt = $saturdayOpenAt;

        return $this;
    }

    public function getSaturdayCloseAt(): ?int
    {
        return $this->saturdayCloseAt;
    }

    public function setSaturdayCloseAt(?int $saturdayCloseAt): self
    {
        $this->saturdayCloseAt = $saturdayCloseAt;

        return $this;
    }

    public function getSundayOpenAt(): ?int
    {
        return $this->sundayOpenAt;
    }

    public function setSundayOpenAt(?int $sundayOpenAt): self
    {
        $this->sundayOpenAt = $sundayOpenAt;

        return $this;
    }

    public function getSundayCloseAt(): ?int
    {
        return $this->sundayCloseAt;
    }

    public function setSundayCloseAt(?int $sundayCloseAt): self
    {
        $this->sundayCloseAt = $sundayCloseAt;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }
}

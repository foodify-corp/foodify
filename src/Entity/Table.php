<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\TableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TableRepository::class)
 * @ORM\Table(name="`table`")
 */
class Table
{
    use TimestampableCarbonTrait;

    const AVAILABLE = "Available";
    const TAKEN = "Taken";
    const PENDING = "Pending";

    /**
     * @Groups("table")
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     */
    private $id;

    /**
     * @Groups("table")
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $table_numero;

    /**
     * @Groups("table")
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $nb_place;

    /**
     * @Groups("table")
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $available;

    /**
     * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="tables")
     * @ORM\JoinColumn(nullable=true)
     */
    private $restaurant_id;

    /**
     * @Groups("table")
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="table_id")
     */
    private $reservations;

    /**
     * @Groups("table")
     * @ORM\Column(type="string", length=255)
     */
    private $lunch_service;

    /**
     * @Groups("table")
     * @ORM\Column(type="string", length=255)
     */
    private $evening_service;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTableNumero(): ?int
    {
        return $this->table_numero;
    }

    public function setTableNumero(int $table_numero): self
    {
        $this->table_numero = $table_numero;

        return $this;
    }

    public function getNbPlace(): ?int
    {
        return $this->nb_place;
    }

    public function setNbPlace(int $nb_place): self
    {
        $this->nb_place = $nb_place;

        return $this;
    }

    public function getAvailable(): ?bool
    {
        return $this->available;
    }

    public function setAvailable(bool $available): self
    {
        $this->available = $available;

        return $this;
    }

    public function getRestaurantId(): ?Restaurant
    {
        return $this->restaurant_id;
    }

    public function setRestaurantId(?Restaurant $restaurant_id): self
    {
        $this->restaurant_id = $restaurant_id;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setTableId($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getTableId() === $this) {
                $reservation->setTableId(null);
            }
        }

        return $this;
    }

    public function getLunchService(): ?string
    {
        return $this->lunch_service;
    }

    public function setLunchService(string $lunch_service): self
    {
        $this->lunch_service = $lunch_service;

        return $this;
    }

    public function getEveningService(): ?string
    {
        return $this->evening_service;
    }

    public function setEveningService(string $evening_service): self
    {
        $this->evening_service = $evening_service;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;
use JetBrains\PhpStorm\Pure;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @Vich\Uploadable()
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface, JWTUserInterface, Serializable
{
    use TimestampableCarbonTrait;

    /**
     * @Groups("user")
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     */
    private string $id;

    /**
     * @Groups("user")
     * @ORM\Column(type="string", length=180, unique=true)
     * @Type("string")
     */
    private string $email;

    /**
     * @Groups("user")
     * @ORM\Column(type="json")
     * @Type("array")
     */
    private array $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private string $password;

    /**
     * @Groups("user")
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Type("string")
     */
    private ?string $firstname;

    /**
     * @Groups("user")
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Type("string")
     */
    private ?string $lastname;

    /**
     * @Groups("user")
     * @ORM\Column(type="datetime", nullable=true)
     * @Type("DateTime")
     */
    private ?DateTimeInterface $birthday;

    /**
     * @Groups("user")
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Type("string")
     */
    private ?string $phone;

    /**
     * @Groups("user")
     * @ORM\Column(type="boolean", options={"default" : true})
     * @Type("boolean")
     */
    private ?bool $is_shown;

    /**
     * @Groups("user")
     * @ORM\Column(type="text", nullable=true)
     * @Type("string")
     */
    private ?string $facebook_url;

    /**
     * @Groups("user")
     * @ORM\Column(type="text", nullable=true)
     * @Type("string")
     */
    private ?string $instagram_url;

    /**
     * @Groups("user")
     * @ORM\Column(type="text", nullable=true)
     * @Type("string")
     */
    private ?string $twitter_url;

    /**
     * @Groups("user")
     * @Vich\UploadableField(mapping="images_avatar", fileNameProperty="avatarName", size="avatarSize")
     *
     * @var File|null
     */
    private $avatarFile;

    /**
     * @Groups("user")
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private ?string $avatarName = null;

    /**
     * @Groups("user")
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private ?int $avatarSize;

    /**
     * @ORM\OneToMany(targetEntity=Restaurant::class, mappedBy="owner_user_id")
     */
    private $restaurants;

    /**
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="user_id", orphanRemoval=true)
     */
    private $reservations;

    /**
     * @ORM\OneToOne(targetEntity=UserWorkAtRestaurant::class, mappedBy="user_id", cascade={"persist", "remove"})
     */
    private $userWorkAtRestaurant;

    /**
<<<<<<< HEAD
     * @Groups("user")
     * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="usersRestaurant")
     */
    private $restaurant;

    /**
     * @ORM\OneToMany(targetEntity=ConnectedUser::class, mappedBy="connected_user")
     */
    private $connectedUsers;

    /*
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="user_id")
     */
    private $comments;

    public function __construct()
    {
        $this->restaurants = new ArrayCollection();
        $this->reservations = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->connectedUsers = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): ?self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getIsShown(): ?bool
    {
        return $this->is_shown;
    }

    public function setIsShown(bool $is_shown): self
    {
        $this->is_shown = $is_shown;

        return $this;
    }

    public function getFacebookUrl(): ?string
    {
        return $this->facebook_url;
    }

    public function setFacebookUrl(?string $facebook_url): self
    {
        $this->facebook_url = $facebook_url;

        return $this;
    }

    public function getInstagramUrl(): ?string
    {
        return $this->instagram_url;
    }

    public function setInstagramUrl(?string $instagram_url): self
    {
        $this->instagram_url = $instagram_url;

        return $this;
    }

    public function getTwitterUrl(): ?string
    {
        return $this->twitter_url;
    }

    public function setTwitterUrl(?string $twitter_url): self
    {
        $this->twitter_url = $twitter_url;

        return $this;
    }

    /**
     * @param File|null $avatarFile
     * @return User
     */
    public function setAvatarFile(?File $avatarFile = null): self
    {
        $this->avatarFile = $avatarFile;

        if ($this->avatarFile instanceof UploadedFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    public function getAvatarFile(): ?File
    {
        return $this->avatarFile;
    }

    public function setAvatarName(?string $avatarName): self
    {
        $this->avatarName = $avatarName;

        return $this;
    }

    public function getAvatarName(): ?string
    {
        return $this->avatarName ?: "";
    }

    public function setAvatarSize(?int $avatarSize): self
    {
        $this->avatarSize = $avatarSize;

        return $this;
    }

    public function getAvatarSize(): ?int
    {
        return $this->avatarSize;
    }

    public static function createFromPayload($username, array $payload): User|JWTUserInterface|null
    {
        return
            (new User())
            ->setId($username)
            ->setRoles($payload['roles'])
            ->setEmail($payload['email'] ?? '')
            ->setFirstname($payload['firstname'] ?? '')
            ->setLastname($payload['lastname'] ?? '')
            ->setAvatarName($payload['avatar_name'] ?? '');
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
        ));
    }

    public function unserialize($data)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
        ) = unserialize($data);
    }

    /**
     * @return Collection|Restaurant[]
     */
    public function getRestaurants(): Collection
    {
        return $this->restaurants;
    }

    public function addRestaurant(Restaurant $restaurant): self
    {
        if (!$this->restaurants->contains($restaurant)) {
            $this->restaurants[] = $restaurant;
            $restaurant->setOwnerUserId($this);
        }

        return $this;
    }

    public function removeRestaurant(Restaurant $restaurant): self
    {
        $this->restaurants->removeElement($restaurant);
//        if ($this->restaurants->removeElement($restaurant)) {
//            // set the owning side to null (unless already changed)
//            if ($restaurant->getOwnerUserId() === $this) {
//                $restaurant->setOwnerUserId(null);
//            }
//        }

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setUserId($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getUserId() === $this) {
                $reservation->setUserId(null);
            }
        }

        return $this;
    }

    public function getUserWorkAtRestaurant(): ?UserWorkAtRestaurant
    {
        return $this->userWorkAtRestaurant;
    }

    public function setUserWorkAtRestaurant(?UserWorkAtRestaurant $userWorkAtRestaurant): self
    {
        // unset the owning side of the relation if necessary
        if ($userWorkAtRestaurant === null && $this->userWorkAtRestaurant !== null) {
            $this->userWorkAtRestaurant->setUserId(null);
        }

        // set the owning side of the relation if necessary
        if ($userWorkAtRestaurant !== null && $userWorkAtRestaurant->getUserId() !== $this) {
            $userWorkAtRestaurant->setUserId($this);
        }

        $this->userWorkAtRestaurant = $userWorkAtRestaurant;

        return $this;
    }
    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * @return Collection<int, ConnectedUser>
     */
    public function getConnectedUsers(): Collection
    {
        return $this->connectedUsers;
    }

    public function addConnectedUser(ConnectedUser $connectedUser): self
    {
        if (!$this->connectedUsers->contains($connectedUser)) {
            $this->connectedUsers[] = $connectedUser;
            $connectedUser->setConnectedUser($this);
        }
        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUserId($this);
        }

        return $this;
    }

    public function removeConnectedUser(ConnectedUser $connectedUser): self
    {
        if ($this->connectedUsers->removeElement($connectedUser)) {
            // set the owning side to null (unless already changed)
            if ($connectedUser->getConnectedUser() === $this) {
                $connectedUser->setConnectedUser(null);
            }
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUserId() === $this) {
                $comment->setUserId(null);
            }
        }

        return $this;
    }

    #[Pure] public function __toString()
    {
        return $this->getEmail();
    }
}

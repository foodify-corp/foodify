<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableCarbonTrait;
use App\Repository\UserWorkAtRestaurantRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UserWorkAtRestaurantRepository::class)
 */
class UserWorkAtRestaurant
{
    use TimestampableCarbonTrait;

    /**
     * @Groups("user_restaurant")
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("doctrine.uuid_generator")
     */
    private $id;

    /**
     * @Groups("user_restaurant")
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="userWorkAtRestaurant", cascade={"persist", "remove"})
     */
    private $user_id;

    /**
     * @Groups("user_restaurant")
     * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="userWorkAtRestaurants")
     */
    private $restaurant_id;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant_id;
    }

    public function setRestaurant(?Restaurant $restaurant_id): self
    {
        $this->restaurant_id = $restaurant_id;

        return $this;
    }
}

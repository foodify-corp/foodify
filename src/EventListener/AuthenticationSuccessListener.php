<?php

namespace App\EventListener;

use App\Entity\ConnectedUser;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

class AuthenticationSuccessListener
{
    protected EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $connectedUserRepository = $this->entityManager->getRepository(ConnectedUser::class);
        $userRepository = $this->entityManager->getRepository(User::class);

        $connectedUser = new ConnectedUser();
        $connectedUser->setUserId($userRepository->findOneBy(["id" => $event->getUser()->getId()]));
        if ($connectedUser->getUserId()) {
            $connectedUserRepository->add($connectedUser);
        }
    }
}
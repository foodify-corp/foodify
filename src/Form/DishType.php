<?php

namespace App\Form;

use App\Entity\Restaurant;
use App\Entity\User;
use App\Repository\UserRepository;
use Faker\Provider\Text;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class DishType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du restaurant'
            ])
            ->add('price', TextType::class, [
                'label' => 'Adresse',
                'required' => true
            ])
            ->add('is_available', TextType::class, [
                'label' => 'Ville',
                'required' => true
            ])
            ->add('is_available_time', IntegerType::class, [
                'label' => 'Code postale',
                'required' => true
            ])
            ->add('image', CountryType::class, [
                'label' => 'Pays',
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Restaurant::class
        ]);
    }
}

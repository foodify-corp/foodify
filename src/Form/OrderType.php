<?php

namespace App\Form;

use App\Entity\Order;
use App\Entity\Restaurant;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\DBAL\Types\BooleanType;
use Faker\Provider\Text;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('order_type', TextType::class, [
                'label' => 'Type de commande',
                'required' => true
            ])
            ->add('order_type', ChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices'  => [
                    'Sur place' => ORDER::ORDER_TYPE_ON_SITE,
                    'à emporter' => ORDER::ORDER_TYPE_TAKE_AWAY,
                ],
            ])
            ->add('status', ChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices'  => [
                    'Accepter' => ORDER::STATUS_ACCEPTED,
                    'Fait' => ORDER::STATUS_DONE,
                    'En attente' => ORDER::STATUS_PENDING,
                ],
            ])
            ->add('user', EntityType::class, [
                'label' => 'Utilisateur',
                'class' => User::class,
                'required' => true,
                'multiple' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class
        ]);
    }
}

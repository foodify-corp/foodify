<?php

namespace App\Form;

use App\Entity\Reservation;
use App\Entity\Restaurant;
use App\Entity\User;
use App\Repository\UserRepository;
use Faker\Provider\Text;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('restaurant_id', TextType::class, [
                'label' => 'Nom du restaurant'
            ])
            ->add('user_id', TextType::class, [
                'label' => 'Adresse',
                'required' => true
            ])
            ->add('table_id', TextType::class, [
                'label' => 'Ville',
                'required' => true
            ])
            ->add('reservation_date', IntegerType::class, [
                'label' => 'Code postale',
                'required' => true
            ])
            ->add('nb_people', CountryType::class, [
                'label' => 'Pays',
                'required' => true
            ])
            ->add('is_validated', TextType::class, [
                'label' => 'Activité',
                'required' => true,
            ])
            ->add('confirmed_at', IntegerType::class, [
                'label' => 'Numéro de Siren',
                'required' => true,
            ])
            ->add('status', TextType::class, [
                'label' => 'Style de cuisine',
                'required' => true,
            ])
            ->add('name', CheckboxType::class, [
                'label' => 'à emporter',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class
        ]);
    }
}

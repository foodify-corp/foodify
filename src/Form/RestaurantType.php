<?php

namespace App\Form;

use App\Entity\Restaurant;
use App\Entity\User;
use App\Repository\UserRepository;
use Faker\Provider\Text;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RestaurantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du restaurant'
            ])
            ->add('address', TextType::class, [
                'label' => 'Adresse',
                'required' => true
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
                'required' => true
            ])
            ->add('cp', IntegerType::class, [
                'label' => 'Code postale',
                'required' => true
            ])
            ->add('country', CountryType::class, [
                'label' => 'Pays',
                'required' => true
            ])
            ->add('activity', TextType::class, [
                'label' => 'Activité',
                'required' => true,
            ])
            ->add('n_siren', IntegerType::class, [
                'label' => 'Numéro de Siren',
                'required' => true,
            ])
            ->add('kitchen_style', TextType::class, [
                'label' => 'Style de cuisine',
                'required' => true,
            ])
            ->add('take_away', CheckboxType::class, [
                'label' => 'à emporter',
                'required' => false,
            ])
            ->add('on_place', CheckboxType::class, [
                'label' => 'Sur place',
                'required' => false,
            ])
            ->add('facebook_url', UrlType::class, [
                'label' => 'Lien Facebook',
                'required' => false
            ])
            ->add('instagram_url', UrlType::class, [
                'label' => 'Lien Instagram',
                'required' => false
            ])
            ->add('twitter_url', UrlType::class, [
                'label' => 'Lien Twitter',
                'required' => false
            ])
            ->add('owner_user_id', EntityType::class, [
                'label' => 'Propriétaire',
                'class' => User::class,
                'required' => true,
                'multiple' => false
            ])
            ->add('restaurantFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => true,
                'download_uri' => true,
                'image_uri' => true,
                'asset_helper' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Restaurant::class,
        ]);
    }
}

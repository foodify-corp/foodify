<?php

namespace App\Repository;

use App\Entity\CategoriesDishes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategoriesDishes|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoriesDishes|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoriesDishes[]    findAll()
 * @method CategoriesDishes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriesDishesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoriesDishes::class);
    }

    // /**
    //  * @return CategoriesDishes[] Returns an array of CategoriesDishes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoriesDishes
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

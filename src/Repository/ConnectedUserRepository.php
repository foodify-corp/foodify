<?php

namespace App\Repository;

use App\Entity\ConnectedUser;
use App\Entity\User;
use App\Service\CustomServiceEntityRepository;
use Carbon\CarbonImmutable;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConnectedUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConnectedUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConnectedUser[]    findAll()
 * @method ConnectedUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConnectedUserRepository extends CustomServiceEntityRepository
{
    const FILTER_NAME = "Utilisateur connectés";
    const FILTER_VALUE = "connected_users";

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConnectedUser::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ConnectedUser $entity, bool $flush = true): void
    {
        $connected = $this->createQueryBuilder('c')
            ->andWhere('c.created_at > :created_at')
            ->andWhere('c.user_id = :user_id')
            ->setParameter('created_at', Carbon::now()->subMinutes(15))
            ->setParameter('user_id', $entity->getUserId())
            ->getQuery()
            ->getResult()
            ;

        if (!$connected) {
            $this->_em->persist($entity);
            if ($flush) {
                $this->_em->flush();
            }
        } else {
            $this->update($entity->getUserId()->getId());
        }
    }

    public function update(string $user_id, bool $flush = true): void
    {
        $entity = $this->findOneBy(["user_id" => $user_id]);

        if ($entity) {
            $entity->setUpdatedAt(Carbon::now());
            $this->_em->persist($entity);
            if ($flush) {
                $this->_em->flush();
            }
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(?ConnectedUser $entity, bool $flush = true): void
    {
        if($entity) {
            $this->_em->remove($entity);
            if ($flush) {
                $this->_em->flush();
            }
        }

    }

    public function getConnectedUser(): int
    {
        return count($this->createQueryBuilder('c')
            ->andWhere('c.created_at > :created_at')
            ->setParameter('created_at', CarbonImmutable::now()->subMinutes(15))
            ->getQuery()
            ->getResult()
        );
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function findQtyLatest(): array
    {
        return $this->createQueryBuilder('c')
            ->select('u')
            ->innerJoin(User::class, 'u', 'WITH', 'u.id = c.user_id')
            ->orderBy('c.created_at', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults(10)
            ->getQuery()
            ->getArrayResult() ;
    }

    // /**
    //  * @return ConnectedUser[] Returns an array of ConnectedUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConnectedUser
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

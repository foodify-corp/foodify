<?php

namespace App\Repository;

use App\Entity\MenuDishes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MenuDishes|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuDishes|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuDishes[]    findAll()
 * @method MenuDishes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuDishesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuDishes::class);
    }
}

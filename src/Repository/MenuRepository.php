<?php

namespace App\Repository;

use App\Entity\Menu;
use App\Service\CustomServiceEntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Menu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Menu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Menu[]    findAll()
 * @method Menu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuRepository extends CustomServiceEntityRepository
{
    const FILTER_NAME = "Menus";
    const FILTER_VALUE = "menus";

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Menu::class);
    }

    public function findAllToArrayByRestaurantId(string $id): array
    {
        return $this->createQueryBuilder('m')
            ->where('m.restaurant_id = :restaurant')
            ->setParameter('restaurant', $id)
            ->getQuery()
            ->getArrayResult() ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function menuCount(): ?int
    {
        return $this->createQueryBuilder('m')
            ->select('count(m.id)')
            ->getQuery()
            ->getSingleScalarResult() ;
    }
}

<?php

namespace App\Repository;

use App\Entity\Order;
use App\Service\CustomServiceEntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends CustomServiceEntityRepository
{
    const FILTER_NAME = "Commandes";
    const FILTER_VALUE = "orders";

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function findTodaysOrdersByRestaurantAndStatus($restaurant, $status)
    {
        return $this->createQueryBuilder('o')
            ->where('o.restaurant = :restaurant')
            ->setParameter('restaurant', $restaurant)
            ->andWhere('o.status = :status')
            ->setParameter('status', $status)
            ->andWhere('o.created_at > :yesterday')
            ->setParameter('yesterday', new \DateTime("yesterday"))
            ->orderBy('o.created_at', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllToArrayByRestaurantId(string $id): array
    {
        return $this->createQueryBuilder('o')
            ->where('o.restaurant = :restaurant')
            ->setParameter('restaurant', $id)
            ->getQuery()
            ->getArrayResult() ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function orderCount(): ?int
    {
        return $this->createQueryBuilder('o')
            ->select('count(o.id)')
            ->getQuery()
            ->getSingleScalarResult() ;
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

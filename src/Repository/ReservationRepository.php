<?php

namespace App\Repository;

use App\Entity\Reservation;
use App\Service\CustomServiceEntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Reservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservation[]    findAll()
 * @method Reservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationRepository extends CustomServiceEntityRepository
{
    const FILTER_NAME = "Réservations";
    const FILTER_VALUE = "reservations";


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reservation::class);
    }

    public function findAllToArrayByRestaurantId(string $id): array
    {
        return $this->createQueryBuilder('r')
            ->where('r.restaurant_id = :restaurant')
            ->setParameter('restaurant', $id)
            ->getQuery()
            ->getArrayResult() ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function reservationCount(): ?int
    {
        return $this->createQueryBuilder('r')
            ->select('count(r.id)')
            ->getQuery()
            ->getSingleScalarResult() ;
    }
}

<?php

namespace App\Repository;

use App\Entity\Restaurant;
use App\Service\CustomServiceEntityRepository;
use Carbon\CarbonImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\String_;

/**
 * @method Restaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Restaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Restaurant[]    findAll()
 * @method Restaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantRepository extends CustomServiceEntityRepository
{
    const FILTER_NAME = "Restaurants";
    const FILTER_VALUE = "restaurants";

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Restaurant::class);
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function restaurantsCount(): ?int
    {
        return $this->createQueryBuilder('r')
            ->select('count(r.id)')
            ->getQuery()
            ->getSingleScalarResult() ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function newRestaurantsCount(int $createdAfter = 24): ?int
    {
        return $this->createQueryBuilder('r')
            ->where('r.created_at > :created_after')
            ->setParameter('created_after', CarbonImmutable::now()->subHours($createdAfter))
            ->select('count(r.id)')
            ->getQuery()
            ->getSingleScalarResult() ;
    }
}

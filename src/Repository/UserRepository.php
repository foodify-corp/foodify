<?php

namespace App\Repository;

use App\Entity\User;
use App\Service\CustomServiceEntityRepository;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends CustomServiceEntityRepository implements PasswordUpgraderInterface
{
    const FILTER_NAME = "Utilisateurs";
    const FILTER_VALUE = "users";

    private ObjectRepository $refreshToken;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findAllToArrayByRestaurantId(string $id): array
    {
        return $this->createQueryBuilder('u')
            ->where('u.restaurant = :restaurant')
            ->setParameter('restaurant', $id)
            ->getQuery()
            ->getArrayResult() ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function usersCount(): ?int
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult() ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function newUsersCount(int $createdAfter = 24): ?int
    {
        return $this->createQueryBuilder('u')
            ->where('u.created_at > :created_after')
            ->setParameter('created_after', CarbonImmutable::now()->subHours($createdAfter))
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult() ;
    }

    public function generateCustomerAccount(UserPasswordHasherInterface $passwordHasher, string $email): string
    {
        $comb = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $tmp_password = substr(str_shuffle($comb),0,15);

        $user = new User();
        $user->setEmail($email);

        $user->setPassword($passwordHasher->hashPassword($user, $tmp_password));
        $this->_em->persist($user);
        $this->_em->flush();

        return $tmp_password;
    }

    public function checkUserExist(string $email): ?User
    {
        return $this->_em->getRepository(User::class)->findOneBy([
            'email' => $email,
        ]);
    }

    public function createAdmin(UserPasswordHasherInterface $passwordHasher, string $email, string $password): void
    {
        if($this->checkUserExist($email)){
            $user = new User();
            $user->setEmail($email);
            $user->setPassword($passwordHasher->hashPassword($user, $password));
            $user->setRoles(["ROLE_ADMIN"]);

            $this->_em->persist($user);
            $this->_em->flush();
        }

        $identifier = "";

        $token = $this->refreshToken->findOneBy(['username' => $identifier]);
        $this->_em->remove($token);
        $this->_em->flush();

        if($token){
            $this->_em->remove($token);
            $this->_em->flush();
        }
    }
}

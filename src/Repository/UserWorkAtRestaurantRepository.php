<?php

namespace App\Repository;

use App\Entity\UserWorkAtRestaurant;
use App\Service\CustomServiceEntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserWorkAtRestaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserWorkAtRestaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserWorkAtRestaurant[]    findAll()
 * @method UserWorkAtRestaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserWorkAtRestaurantRepository extends CustomServiceEntityRepository
{
    const FILTER_NAME = "Utilisateurs";
    const FILTER_VALUE = "restaurant_users";

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserWorkAtRestaurant::class);
    }

    public function findUserWorkAtRestaurant($user, $restaurant)
    {
        return $this->createQueryBuilder('uwar')
            ->where('uwar.restaurant_id = :restaurant')
            ->setParameter('restaurant', $restaurant)
            ->andWhere('uwar.user_id = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}

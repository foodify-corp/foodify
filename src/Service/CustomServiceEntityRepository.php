<?php

namespace App\Service;

use App\Entity\Restaurant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

class CustomServiceEntityRepository extends ServiceEntityRepository
{
    private string $entityClass;
    public function __construct(ManagerRegistry $registry, string $entityClass)
    {
        $this->entityClass = $entityClass;
        parent::__construct($registry, $this->entityClass);
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function findQtyLatest(): array
    {
        return $this->createQueryBuilder('e')
            ->orderBy('e.created_at', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults(10)
            ->getQuery()
            ->getArrayResult() ;
    }

    public function findQtyLatestByRestaurant(Restaurant $restaurant, string $col): array
    {
        return $this->createQueryBuilder('e')
            ->where('e.' . $col . ' = :restaurant')
            ->setParameter('restaurant', $restaurant->getId())
            ->orderBy('e.created_at', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults(10)
            ->getQuery()
            ->getArrayResult();
    }

    public function entityCount(): ?int
    {
        return $this->createQueryBuilder('e')
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult() ;
    }

    public function findAllToArray(): array
    {
        return $this->createQueryBuilder('e')
            ->getQuery()
            ->getArrayResult() ;
    }

    public function findAllToArrayByRestaurantId(string $id): array
    {
        return $this->createQueryBuilder('e')
            ->where('e.restaurant_id = :restaurant')
            ->setParameter('restaurant', $id)
            ->getQuery()
            ->getArrayResult() ;
    }

    public function findAllToArrayByParameters(array $parameters): array
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $parameterSet = 0;

        foreach ( $parameters as $index => $parameter ) {
            if ( !empty($parameter)) {
                if ( $parameterSet === 0 ) {
                    if ($index === 'roles' ) {
                        $queryBuilder
                            ->where("JSON_GET_TEXT(e.roles,0) = :" . $index . "")
                            ->setParameter($index, $parameter);
                    } else {
                        $queryBuilder
                            ->where('e.' . $index . ' =:' . $index)
                            ->setParameter($index, $parameter);
                    }
                } else {

                    if ($index === 'roles' ) {
                        $queryBuilder
                            ->orWhere("JSONB_CONTAINS(e." . $index . ", :" . $index . ") = 1")
                            ->setParameter($index, $parameter);
                    } else {
                        $queryBuilder
                            ->orWhere('e.' . $index . ' = :' . $index)
                            ->setParameter($index, $parameter);
                    }
                }

                $parameterSet++;
            }
        }

        return $queryBuilder
            ->getQuery()
            ->getArrayResult() ;
    }

    public function findAllToArrayByParametersByRestaurant(array $parameters, string $restaurant_id): array
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $parameterSet = 0;

        foreach ( $parameters as $index => $parameter ) {
            if ( !empty($parameter)) {
                if ( $parameterSet === 0 ) {
                    if ($index === 'roles' ) {
                        $queryBuilder
                            ->where("JSON_GET_TEXT(e.roles,0) = :" . $index . "")
                            ->setParameter($index, $parameter);
                    } else {
                        $queryBuilder
                            ->where('e.' . $index . ' =:' . $index)
                            ->setParameter($index, $parameter);
                    }
                } else {

                    if ($index === 'roles' ) {
                        $queryBuilder
                            ->orWhere("JSONB_CONTAINS(e." . $index . ", :" . $index . ") = 1")
                            ->setParameter($index, $parameter);
                    } else {
                        $queryBuilder
                            ->orWhere('e.' . $index . ' = :' . $index)
                            ->setParameter($index, $parameter);
                    }
                }

                $queryBuilder
                    ->andWhere("restaurant_id = :restaurant_id")
                    ->setParameter("restaurant_id", $restaurant_id);

                $parameterSet++;
            }
        }

        return $queryBuilder
            ->getQuery()
            ->getArrayResult() ;
    }
}
<?php

namespace App\Service;

use App\Entity\ConnectedUser;
use App\Entity\Dish;
use App\Entity\Menu;
use App\Entity\Order;
use App\Entity\Reservation;
use App\Entity\Restaurant;
use App\Entity\User;
use App\Repository\ConnectedUserRepository;
use App\Repository\DishRepository;
use App\Repository\MenuRepository;
use App\Repository\OrderRepository;
use App\Repository\ReservationRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use Carbon\CarbonImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\UX\Chartjs\Model\Chart;

class DashboardConfigService
{
    const MONTHS = "months";
    const DAYS = "days";
    const HOURS = "hours";
    const MINUTES = "minutes";

    private Chart $chart;

    private array $label_filter = [];
    private array $data = [];

    private array $selection = [];
    private array $filter = [
        [
            "name" => "Par mois",
            "value" => self::MONTHS
        ],
        [
            "name" => "Par jours",
            "value" => self::DAYS
        ],
        [
            "name" => "Par heures",
            "value" => self::HOURS
        ],
        [
            "name" => "Par minutes",
            "value" => self::MINUTES
        ],
    ];

    private array $repository_selection = [
        ConnectedUserRepository::FILTER_VALUE => ConnectedUser::class,
        OrderRepository::FILTER_VALUE => Order::class,
        ReservationRepository::FILTER_VALUE => Reservation::class,
        RestaurantRepository::FILTER_VALUE => Restaurant::class,
        DishRepository::FILTER_VALUE => Dish::class,
        UserRepository::FILTER_VALUE => User::class,
        MenuRepository::FILTER_VALUE => Menu::class,
    ];

    private EntityManagerInterface $em;

    private array $bottom_selection = [];

    private string $chart_selected;
    private string $chart_filter;

    public function __construct(EntityManagerInterface $em, Chart $chart, string $chart_selected, string $chart_filter)
    {
        $this->em = $em;
        $this->chart = $chart;
        $this->chart_selected = $chart_selected;
        $this->chart_filter = $chart_filter;
    }

    public function addSelection(string $name, string $value): void
    {
        $this->selection = [
            'name' => $name,
            'value' => $value
        ];
    }

    public function addSelectionRange(array $selection): void
    {
        $this->selection = array_merge($this->selection, $selection);
    }

    public function getSelection(): array
    {
        return $this->selection;
    }

    public function addBottomSelection(string $key, string $value): void
    {
        $this->bottom_selection = [
            $key => $value
        ];
    }

    public function addBottomSelectionRange(array $bottom_selection): void
    {
        $this->bottom_selection = array_merge($this->bottom_selection, $bottom_selection);
    }

    public function getBottomSelection(): array
    {
        return $this->bottom_selection;
    }

    public function getFilter(): array
    {
        return $this->filter;
    }

    public function setChartDefaultFilteredData(): void
    {
        switch(strtolower($this->chart_filter)) {
            case self::MONTHS:
                $this->label_filter = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                for ($i = 1; $i <= 12; $i++) {
                    array_push($this->data, $i);
                }
                break;
            case self::DAYS:
                for ($i = 1; $i <= 31; $i++) {
                    array_push($this->label_filter, $i);
                    array_push($this->data, $i);
                }
                break;
            case self::HOURS:
                for ($i = 0; $i < 24; $i++) {
                    array_push($this->label_filter, $i);
                    array_push($this->data, $i);
                }
                break;
            case self::MINUTES:
                for ($i = 0; $i < 60; $i++) {
                    array_push($this->data, $i);
                    array_push($this->label_filter, $i);
                }
                break;
            default:
                break;
        }
    }

    public function getDefaultChart(): Chart
    {
        $this->setChartDefaultFilteredData();

        $this->chart->setData([
            'labels' => $this->label_filter,
            'datasets' => [
                [
                    'label' => 'My First dataset',
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $this->data,
                ],
            ],
        ]);

        $this->chart->setOptions([
            'scales' => [
                'y' => [
                    'suggestedMin' => 0,
                ],
            ],
        ]);

        return $this->chart;
    }

    public function setChart(int $years, int $months, int $days, int $hours): void
    {
        $repository = $this->em->getRepository($this->repository_selection[$this->chart_selected]);
        $value = array();

        switch (strtolower($this->chart_filter)) {
            case self::MONTHS:
                $date = CarbonImmutable::createFromDate($years, 1, 1)->hours(0)->minutes(0);;
                $this->label_filter = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

                for ($i = 0 ; $i < 12; $i++) {
                    $month = $repository->createQueryBuilder('c')
                        ->andWhere('c.created_at < :created_at_plus')
                        ->andWhere('c.created_at > :created_at_minus')
                        ->setParameter('created_at_plus', $date->addMonths($i + 1))
                        ->setParameter('created_at_minus', $date->addMonths($i))
                        ->getQuery()
                        ->getResult();
                    $value[] = count($month);
                }

                $this->data = $value;
                break;

            case self::DAYS:
                $date = CarbonImmutable::createFromDate($years, $months, 1)->hours(0)->minutes(0);
                $last_day = $date->endOfMonth()->day;

                for ($i = 0; $i < $last_day; $i++) {
                    array_push($this->label_filter, $i + 1 );

                    $day = $repository->createQueryBuilder('c')
                        ->andWhere('c.created_at < :created_at_plus')
                        ->andWhere('c.created_at > :created_at_minus')
                        ->setParameter('created_at_plus', $date->addDays($i + 1))
                        ->setParameter('created_at_minus', $date->addDays($i))
                        ->getQuery()
                        ->getResult();
                    $value[] = count($day);
                }

                $this->data = $value;
                break;
            case self::HOURS:
                $date = CarbonImmutable::createFromDate($years, $months, $days)->hours(0)->minutes(0);

                for ($i = 0; $i < 24; $i++) {
                    array_push($this->label_filter, $i);

                    $day = $repository->createQueryBuilder('c')
                        ->andWhere('c.created_at < :created_at_plus')
                        ->andWhere('c.created_at > :created_at_minus')
                        ->setParameter('created_at_plus', $date->addHours($i + 1))
                        ->setParameter('created_at_minus', $date->addHours($i))
                        ->getQuery()
                        ->getResult();
                    $value[] = count($day);
                }

                $this->data = $value;
                break;
            case self::MINUTES:
                $date = CarbonImmutable::createFromDate($years, $months, $days)->hours($hours)->minutes(0);

                for ($i = 0; $i < 60; $i++) {
                    array_push($this->label_filter, $i);

                    $day = $repository->createQueryBuilder('c')
                        ->andWhere('c.created_at < :created_at_plus')
                        ->andWhere('c.created_at > :created_at_minus')
                        ->setParameter('created_at_plus', $date->addMinutes($i + 1))
                        ->setParameter('created_at_minus', $date->addMinutes($i))
                        ->getQuery()
                        ->getResult();
                    $value[] = count($day);
                }

                $this->data = $value;
                break;
            default:
                break;
        }

        $this->chart->setData([
            'labels' => $this->label_filter, // nom des bar
            'datasets' => [
                [
                    'label' => 'My First dataset',
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $this->data,
                ],
            ],
        ]);

        $this->chart->setOptions([
            'scales' => [
                'y' => [
                    'suggestedMin' => 0,
                ],
            ],
        ]);
    }

    public function getChart(): Chart
    {
        return $this->chart;
    }

    public function getLatestValue() {
        $repository = $this->em->getRepository($this->repository_selection[$this->chart_selected]);

        return $repository->findQtyLatest();
    }

    public function getLatestValueByRestaurant(Restaurant $restaurant, string $col) {
        $repository = $this->em->getRepository($this->repository_selection[$this->chart_selected]);

        return $repository->findQtyLatestByRestaurant($restaurant, $col);
    }
}
module.exports = {
  purge: {
    content: ['./templates/**/*.html.twig']
  },
  media: false, // or 'media' or 'class'
  theme: {},
  variants: {
    extend: {},
  },
  plugins: [],
}
